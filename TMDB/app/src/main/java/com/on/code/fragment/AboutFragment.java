package com.on.code.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.on.code.BaseFragment;
import com.on.code.R;

@SuppressLint("NewApi")
public class AboutFragment extends BaseFragment implements OnClickListener {

	private TextView lblTitle, lblLinkHome;
	private Activity self;
	private ImageView btnTopMenu;

	public static AboutFragment newInstance() {

		Bundle args = new Bundle();

		AboutFragment fragment = new AboutFragment();
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_about, container, false);
		self = getActivity();
		initUI(view);
		initControl();
		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	public void initUI(View view) {
		lblTitle = (TextView) view.findViewById(R.id.lblTopTitle);
		lblTitle.setText(getResources().getString(R.string.menuAbout));
		btnTopMenu = (ImageView) view.findViewById(R.id.btnTopLeft);
		lblLinkHome = (TextView) view.findViewById(R.id.lblLinkHome);

	}

	private void initControl() {

		lblLinkHome.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Uri uri = Uri.parse(getResources().getString(
						R.string.about_link));
				Intent i = new Intent(Intent.ACTION_VIEW, uri);
				startActivity(i);
			}
		});
	}

	@Override
	public void onClick(View v) {

	}

}
