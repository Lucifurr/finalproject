package com.on.code.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.on.code.BaseActivity;
import com.on.code.R;
import com.on.code.adapter.MenuAdapter;
import com.on.code.config.GlobalValue;
import com.on.code.fragment.AboutFragment;
import com.on.code.fragment.BookMarkFragment;
import com.on.code.fragment.CategoryFragment;
import com.on.code.fragment.MostViewedFragment;
import com.on.code.fragment.NewBookFragment;
import com.on.code.fragment.SearchFragment;
import com.on.code.fragment.UpdatedBookFragment;
import com.on.code.object.MenuObj;

import java.util.ArrayList;

@SuppressLint("NewApi")
public class MainSherkLockActivity extends BaseActivity {

    // Within which the entire activity is enclosed
    DrawerLayout mDrawerLayout;
    // ListView represents Navigation Drawer
    ListView lsvMenu;
    ActionBarDrawerToggle mDrawerToggle;
    // Title of the action bar
    String mTitle = "";
    private MenuAdapter menulistadapter;
    public ArrayList<MenuObj> listmenu;

    public int currentMenu = 0;

    private Fragment mCurrenFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_sherklock);
        initUI();
        // updateScreenOn();
        initMenu();
        initData();
        mTitle = (String) getTitle();
        updateContent(currentMenu);
    }

    /**
     * Menu left onClick and switch fragment
     */
    private void switchScreen(int position) {
        String tag = "frag_" + position;
        mCurrenFragment = getSupportFragmentManager().findFragmentByTag(tag);
        if (mCurrenFragment == null) {
            switch (position) {
                case 0:
                    mCurrenFragment = NewBookFragment.newInstance();
                    break;
//                case 1:
//                    mCurrenFragment = UpdatedBookFragment.newInstance();
//                    break;
                case 1:
                    mCurrenFragment = MostViewedFragment.newInstance();
                    break;
                case 2:
                    mCurrenFragment = CategoryFragment.newInstance();
                    break;
                case 3:
                    mCurrenFragment = SearchFragment.newInstance();
                    break;
                case 4:
                    mCurrenFragment = BookMarkFragment.newInstance();
                    break;
//                case 6:
//                    mCurrenFragment = HistoryFragment.newInstance();
//
//                    break;

                case 5:
                    mCurrenFragment = AboutFragment.newInstance();

                    break;
                case 6:
                    break;
            }

        }
        if (position != 6) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.content_frame, mCurrenFragment, tag);
            fragmentTransaction.addToBackStack(tag);
            fragmentTransaction.commit();
        }
    }

    private void initUI() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        lsvMenu = (ListView) findViewById(R.id.drawer_list);

    }

    private void initMenu() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.ic_menu, R.string.drawer_open, R.string.drawer_close) {

            /** Called when drawer is closed */
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu();
                getActionBar().setTitle(mTitle);
            }

            /** Called when a drawer is opened */
            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle("Menus");
                invalidateOptionsMenu();
            }

        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setBackgroundDrawable(
                new ColorDrawable(getResources().getColor(R.color.bg_header)));
        lsvMenu.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                updateContent(position);
            }

        });
    }

    private void updateContent(int position) {
        // TODO Auto-generated method stub
        currentMenu = position;
        // Currently selected river
        if (position != 6){
            mTitle = listmenu.get(currentMenu).getTitle();
            setActiveMenu(position);
            // Closing the drawer
            mDrawerLayout.closeDrawer(lsvMenu);
            getActionBar().setTitle(mTitle);
            switchScreen(position);
        }else {
            mDrawerLayout.closeDrawer(lsvMenu);
            switchScreen(position);
        }


    }

    public void setActiveMenu(int index) {
        for (int i = 0; i < listmenu.size(); i++) {
            if (index == 6) //rate app
                break;
            if (i == index)
                listmenu.get(i).setSelect(true);
            else
                listmenu.get(i).setSelect(false);
        }
        menulistadapter.notifyDataSetChanged();
    }

    private void initData() {

        // menu list
        listmenu = new ArrayList<>();
        listmenu.add(new MenuObj(getString(R.string.menuNewBook),
                R.drawable.ic_new, true));
//        listmenu.add(new MenuObj(getString(R.string.menuHot),
//                R.drawable.ic_hot, true));
        listmenu.add(new MenuObj(getString(R.string.menuMostViewed),
                R.drawable.ic_recomended, true));
        listmenu.add(new MenuObj(getString(R.string.menuCategory),
                R.drawable.ic_genres, true));

        listmenu.add(new MenuObj(getString(R.string.menuSearch),
                R.drawable.ic_search_white, false));
        listmenu.add(new MenuObj(getString(R.string.menuBookMark),
                R.drawable.ic_bookmark, false));
//        listmenu.add(new MenuObj(getString(R.string.menuHistory),
//                R.drawable.ic_history, true));


        listmenu.add(new MenuObj(getString(R.string.menuAbout),
                R.drawable.ic_about, false));


        menulistadapter = new MenuAdapter(this, listmenu);
        lsvMenu.setAdapter(menulistadapter);

    }

    public void updateScreenOn() {
        if (GlobalValue.preferences.isScreenOn()) {
            getWindow()
                    .addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            Log.e("aaaaaaa", "aaaa screen on");
        } else {
            getWindow()
                    .clearFlags(
                            android.view.WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            Log.e("aaaaaaa", "aaaa screen off");
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    /**
     * Handling the touch event of app icon
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        if (item.getItemId() == R.id.action_search) {
            startActivity(new Intent(getBaseContext(), SearchActivity.class));
//            updateContent(4);
        }

        if (item.getItemId() == R.id.action_change_show_list) {
            if (currentMenu == 0) {
                ((NewBookFragment) mCurrenFragment).updateShowView(NewBookFragment.isGridView);
                if (NewBookFragment.isGridView) {
                    item.setIcon(R.drawable.ic_grid);
                } else {
                    item.setIcon(R.drawable.ic_list);
                }

            }
            if (currentMenu == 1) {
                ((UpdatedBookFragment) mCurrenFragment)
                        .updateShowView(UpdatedBookFragment.isGridView);
                if (UpdatedBookFragment.isGridView) {
                    item.setIcon(R.drawable.ic_grid);
                } else {
                    item.setIcon(R.drawable.ic_list);
                }

            }
            if (currentMenu == 2) {
                ((MostViewedFragment) mCurrenFragment)
                        .updateShowView(MostViewedFragment.isGridView);
                if (MostViewedFragment.isGridView) {
                    item.setIcon(R.drawable.ic_grid);
                } else {
                    item.setIcon(R.drawable.ic_list);
                }

            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Called whenever we call invalidateOptionsMenu()
     */
    @Override
    public boolean onPrepareOptionsMenu(android.view.Menu menu) {
        // If the drawer is open, hide action items related to the content view
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
//        Fragment fragment = getSupportFragmentManager().findFragmentByTag("frag_0"); // 0 is home
//        if (fragment != null && fragment.isVisible()) {
        showQuitDialog();
//        } else {
//            super.onBackPressed();
//        }
    }


    private void showQuitDialog() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.title_check_quit_app)
                .setMessage(R.string.check_quit_app)
                .setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                android.os.Process
                                        .killProcess(android.os.Process.myPid());
                                System.exit(1);
                                finish();
                            }
                        }).setNegativeButton(android.R.string.cancel, null)
                .create().show();

    }

}
