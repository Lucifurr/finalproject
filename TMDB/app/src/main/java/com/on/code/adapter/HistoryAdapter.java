package com.on.code.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.on.code.R;
import com.on.code.activity.DetailActivity;
import com.on.code.activity.MainSherkLockActivity;
import com.on.code.config.Constant;
import com.on.code.config.GlobalValue;
import com.on.code.object.Movie;
import com.on.code.util.MySharedPreferences;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class HistoryAdapter extends BaseAdapter {
    private Activity mainActivity;
    private List<Movie> listVerses;
    private LayoutInflater layoutInflater;
    private AQuery aq;
    public boolean isHistory = false;


    public HistoryAdapter(Activity activity,
                          ArrayList<Movie> listverseVerses) {
        this.mainActivity = activity;
        this.listVerses = listverseVerses;
        this.layoutInflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        aq = new AQuery(mainActivity);

    }

    public int getCount() {
        return listVerses.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        // TODO Auto-generated method stub
        final Hoder holder;
        if (convertView == null) {
            holder = new Hoder();
            convertView = layoutInflater.inflate(R.layout.item_list_main, null);
            holder.lblChapterTitle = (TextView) convertView
                    .findViewById(R.id.lblChapterTitle);
            holder.lblChapterTitle.setSelected(true);
            holder.lblBookTitle = (TextView) convertView
                    .findViewById(R.id.lblBookTitle);
            holder.imgBook = (ImageView) convertView
                    .findViewById(R.id.imgBookIcon);
            holder.lblDesc = (TextView) convertView.findViewById(R.id.lblDesc);
            holder.lblYear = (TextView) convertView
                    .findViewById(R.id.lblYear);
            holder.rbVote = (RatingBar) convertView.findViewById(R.id.rbVoting);
            convertView.setTag(holder);

        } else {
            holder = (Hoder) convertView.getTag();
        }

        final Movie movie = listVerses.get(position);
        if (movie != null) {
            if (GlobalValue.preferences != null) {
                GlobalValue.setTypeFont(holder.lblBookTitle, holder.lblDesc,
                        holder.lblChapterTitle, holder.lblYear
                );
            }


            MySharedPreferences share = new MySharedPreferences(mainActivity);
            String desc = share.getStringValue(Constant.DESC + movie.getId());
            String tag = share.getStringValue(Constant.TAGMOVIE + movie.getId());
            holder.lblChapterTitle.setText(movie.getTitle());
            holder.lblYear.setText(movie.getAuthor().substring(0, 4));
            holder.rbVote.setRating((float) movie.getVoteAverage() / 2);

            LayerDrawable stars = (LayerDrawable) holder.rbVote.getProgressDrawable();
            stars.getDrawable(2).setColorFilter(mainActivity.getResources().getColor(R.color.all_color), PorterDuff.Mode.SRC_ATOP);

            holder.lblBookTitle.setText(tag);
            holder.lblDesc.setText(desc);

//            Log.e("url histories", "1: "+movie.getImage() +" \n 2: "+movie.getOriginalImage());
            Picasso.with(mainActivity).load(movie.getOriginalImage()).into(holder.imgBook);

        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GlobalValue.movie = new Movie();
                GlobalValue.imagebook = movie.getImage();
                Bundle b = new Bundle();
                b.putParcelable(DetailActivity.KEY_OBJECT, movie);
                b.putBoolean("NON_NEW", true);
                ((MainSherkLockActivity) mainActivity).startActivity(DetailActivity.class, b);
            }
        });

        return convertView;


    }


    static class Hoder {
        RatingBar rbVote;
        TextView lblChapterTitle, lblBookTitle, lblDesc, lblYear;
        ImageView imgBook;
    }
}
