package com.on.code.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.on.code.BaseFragment;
import com.on.code.R;
import com.on.code.activity.DetailActivity;
import com.on.code.activity.SearchActivity;
import com.on.code.adapter.MovieGridviewAdapter;
import com.on.code.adapter.BookListViewAdapter;
import com.on.code.config.Constant;
import com.on.code.config.GlobalValue;
import com.on.code.database.DatabaseUtility;
import com.on.code.modelmanager.ParserUtility;
import com.on.code.network.ConnectServer;
import com.on.code.object.Movie;
import com.on.code.pulltorefresh.library.PullToRefreshBase;
import com.on.code.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.on.code.pulltorefresh.library.PullToRefreshGridView;
import com.on.code.pulltorefresh.library.PullToRefreshListView;
import com.on.code.retrofitobj.BaseMovieRespond;
import com.on.code.util.MySharedPreferences;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("NewApi")
public class NewBookFragment extends BaseFragment implements OnClickListener, BookListViewAdapter.IOnClickItemListener {

    private TextView lblTitle;
    private ImageView btnTopMenu, btnSearch, btnChange;
    private GridView grvBook;
    private ListView lsvBook;
    private PullToRefreshListView mListView;
    private PullToRefreshGridView mGridView;
    private MovieGridviewAdapter grv_adapter;
    private BookListViewAdapter lsv_adapter;
    private Activity self;
    private ArrayList<Movie> arrMovies;
    public static boolean isGridView = true;
    // private int valuepage = 1;
    private int currentPage = 1;
    private int totalPage;
    private boolean hasMoreData;// check if there is more data or not
    private int currentView;
    private final int LIST_VIEW = 1;
    private final int GRID_VIEW = 2;
    private String desc = "";


    public static NewBookFragment newInstance() {

        Bundle args = new Bundle();

        NewBookFragment fragment = new NewBookFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        currentView = GRID_VIEW;
        arrMovies = new ArrayList<Movie>();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_book, container,
                false);
        self = getActivity();
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        // Log.e("Movie", "epub: " + epubs.get(1));
        initUI(view);
        initControl();
        hasMoreData = true;
        currentPage = 1;
        if (arrMovies.isEmpty())
            setData(true, currentView);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        // hasMoreData = true;
        // currentPage = 1;
        // arrMovies.clear();
        // setData(true, currentView);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Auto-generated method stub
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.main, menu);
    }

    private void initUI(View view) {
        lblTitle = (TextView) view.findViewById(R.id.lblTopTitle);
        btnTopMenu = (ImageView) view.findViewById(R.id.btnTopLeft);
        btnSearch = (ImageView) view.findViewById(R.id.btnTopRight);
        btnChange = (ImageView) view.findViewById(R.id.btnChange);
        mGridView = (PullToRefreshGridView) view.findViewById(R.id.grvBook);
        mListView = (PullToRefreshListView) view.findViewById(R.id.lsvBook);

        lsvBook = mListView.getRefreshableView();
        grvBook = mGridView.getRefreshableView();
        mGridView.setOnRefreshListener(new OnRefreshListener2<GridView>() {

            @Override
            public void onPullDownToRefresh(
                    PullToRefreshBase<GridView> refreshView) {
                currentView = GRID_VIEW;
                hasMoreData = true;
                currentPage = 1;
                arrMovies.clear();
                setData(true, currentView);

            }

            @Override
            public void onPullUpToRefresh(
                    PullToRefreshBase<GridView> refreshView) {
                currentView = GRID_VIEW;
                currentPage++;
                if (currentPage > totalPage) {
                    hasMoreData = false;
                }
                setData(false, currentView);
            }

        });
        //

        mListView.setOnRefreshListener(new OnRefreshListener2<ListView>() {

            @Override
            public void onPullDownToRefresh(
                    PullToRefreshBase<ListView> refreshView) {
                currentView = LIST_VIEW;
                hasMoreData = true;
                currentPage = 1;
                arrMovies.clear();
                setData(true, currentView);
            }

            @Override
            public void onPullUpToRefresh(
                    PullToRefreshBase<ListView> refreshView) {
                currentView = LIST_VIEW;
                currentPage++;
                if (currentPage > totalPage) {
                    hasMoreData = false;
                }
                setData(false, currentView);
            }
        });
        //
    }

    private void initControl() {
        btnTopMenu.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
        btnChange.setOnClickListener(this);
        setGridView();
    }

    private void setGridView() {
        grv_adapter = new MovieGridviewAdapter(self, arrMovies);
        grvBook.setAdapter(grv_adapter);
        grvBook.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                //countViewBook(arrMovies.get(position).getId());
                if (DatabaseUtility.checkExistsBook(getActivity(), arrMovies
                        .get(position).getId())) {
                    Log.d(TAG, "onItemClick: " + "data da co du lieu");
                    Log.e("", "DATA " + "data da co du lieu");
                } else {
                    DatabaseUtility.insertHistory(getActivity(),
                            arrMovies.get(position));
                    MySharedPreferences share = new MySharedPreferences(getActivity().getBaseContext());
                    share.putStringValue(Constant.DESC + arrMovies.get(position).getId(), arrMovies.get(position).getDesc());
                    share.putStringValue(Constant.TAGMOVIE + arrMovies.get(position).getId(), GlobalValue.genresIdsToString(arrMovies.get(position).getGenreIds()));
                }

                GlobalValue.movie = arrMovies.get(position);
                GlobalValue.imagebook = arrMovies.get(position).getImage();
                Bundle b = new Bundle();
                b.putParcelable(DetailActivity.KEY_OBJECT, arrMovies.get(position));
                startActivity(DetailActivity.class, b);
            }
        });
        lsv_adapter = new BookListViewAdapter(self, arrMovies, this);
        lsvBook.setAdapter(lsv_adapter);
        /*lsvBook.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                //countViewBook(arrMovies.get(position - 1).getId());

            }
        });*/

        updateShowView(isGridView);
    }

    @Override
    public void onItemClicked(int position) {
        if (DatabaseUtility.checkExistsBook(getActivity(), arrMovies
                .get(position).getId())) {
            Log.e("", "DATA " + "data da co du lieu");
        } else {
            DatabaseUtility.insertHistory(getActivity(),
                    arrMovies.get(position));
        }

        //GlobalValue.movie = new Movie();
        GlobalValue.movie = arrMovies.get(position);
        GlobalValue.imagebook = arrMovies.get(position)
                .getImage();
        Bundle b = new Bundle();
        b.putParcelable(DetailActivity.KEY_OBJECT, arrMovies.get(position));

        MySharedPreferences share = new MySharedPreferences(getActivity().getBaseContext());
        share.putStringValue(Constant.DESC + arrMovies.get(position).getId(), arrMovies.get(position).getDesc());
        share.putStringValue(Constant.TAGMOVIE + arrMovies.get(position).getId(), GlobalValue.genresIdsToString(arrMovies.get(position).getGenreIds()));

        startActivity(DetailActivity.class, b);
    }


    public void updateShowView(boolean isGridView) {
        this.isGridView = !isGridView;
        if (isGridView) {
            mListView.setVisibility(View.GONE);
            mGridView.setVisibility(View.VISIBLE);
        } else {
            mGridView.setVisibility(View.GONE);
            mListView.setVisibility(View.VISIBLE);
        }
    }

    private void setData(boolean isRefresh, int mode) {
        if (hasMoreData) {
            lblTitle.setText(self.getString(R.string.menuBook));
            showDialog();
            ConnectServer.getResponseAPI().getMovies(Constant.API_KEY, currentPage + "").enqueue(new Callback<BaseMovieRespond>() {
                @Override
                public void onResponse(Call<BaseMovieRespond> call, Response<BaseMovieRespond> response) {
                    String json = new Gson().toJson(response.body());
                    ArrayList<Movie> arr = ParserUtility
                            .parseListNewBooks(json);
                    totalPage = ParserUtility.getTotalBookPage(json);
                    arrMovies.addAll(arr);
                    grv_adapter.notifyDataSetChanged();
                    lsv_adapter.notifyDataSetChanged();

                    switch (currentView) {
                        case GRID_VIEW:
                            mGridView.onRefreshComplete();
                            break;
                        case LIST_VIEW:
                            mListView.onRefreshComplete();
                            break;
                        default:
                            break;
                    }
                    closeDialog();
                }

                @Override
                public void onFailure(Call<BaseMovieRespond> call, Throwable t) {
                    switch (currentView) {
                        case GRID_VIEW:
                            mGridView.onRefreshComplete();
                            break;
                        case LIST_VIEW:
                            mListView.onRefreshComplete();
                            break;
                        default:
                            break;
                    }
                    closeDialog();
                    Toast.makeText(self, t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });
        } else {

            Toast.makeText(getActivity(), "No more data to show.",
                    Toast.LENGTH_SHORT).show();

            switch (currentView) {
                case GRID_VIEW:
                    mGridView.onRefreshComplete();
                    break;
                case LIST_VIEW:
                    Log.e("LIST_VIEW", "LIST_VIEW");
                    mListView.onRefreshComplete();
                    break;
                default:
                    break;
            }

        }

    }

    @Override
    public void onClick(View v) {

        if (v == btnSearch) {
            startActivity(SearchActivity.class);
            return;
        }
        if (v == btnChange) {
            Toast.makeText(getActivity(), "Onclick",
                    Toast.LENGTH_SHORT).show();
            isGridView = !isGridView;
            updateShowView(isGridView);
            return;
        }
    }

    private void startActivity(Class<?> cls, Bundle bundle) {
        Intent intent = new Intent(self, cls);
        intent.putExtras(bundle);
        startActivity(intent);
        self.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    private void startActivity(Class<?> cls) {
        Intent intent = new Intent(self, cls);
        startActivity(intent);
        self.overridePendingTransition(R.anim.slide_in_left,
                R.anim.slide_out_left);
    }


}
