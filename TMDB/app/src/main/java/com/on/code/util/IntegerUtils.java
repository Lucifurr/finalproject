package com.on.code.util;

import java.util.ArrayList;

import android.util.Log;

import com.on.code.object.Chapter;

public class IntegerUtils {
	private static final String TAG = "IntegerUtils";
	public static int[] getPosition(ArrayList<Chapter> arrChapters, int index) {
		ArrayList<Integer> arr = new ArrayList<Integer>();
		StringBuilder sb = new StringBuilder();
		for (Chapter chapter : arrChapters) {
			sb.append(chapter.getId()+"\n");
			arr.add(Integer.parseInt(chapter.getId()));
		}
		Log.i(TAG, sb.toString());
		int[] arrInt = new int[2];
		if(arr.contains(index))
		{
			int position = arr.indexOf(index);
			if(position == 0){
				arrInt[0] = arr.get(0);
				arrInt[1] = arr.get(1);
			}else if(position == (arr.size() -1)){
				arrInt[0] = arr.get(position-1);
				arrInt[1] = arr.get(position);
			}else{
				arrInt[0] = arr.get(position-1);
				arrInt[1] = arr.get(position+1);
			}
		}
		return arrInt;
	}
}
