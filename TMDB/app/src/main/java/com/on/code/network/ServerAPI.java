package com.on.code.network;


import com.on.code.retrofitobj.BaseMovieRespond;
import com.on.code.retrofitobj.GenreRespond;
import com.on.code.retrofitobj.MovieCreditRespond;
import com.on.code.retrofitobj.MovieDetailRespond;
import com.on.code.retrofitobj.MovieInGenreRespond;
import com.on.code.retrofitobj.VideoMovieRespond;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface ServerAPI {
    @GET("movie/now_playing")
    Call<BaseMovieRespond> getMovies(@Query("api_key") String apiKey,
                                     @Query("page") String page);

    @GET("movie/popular")
    Call<BaseMovieRespond> getHot(@Query("api_key") String apiKey,
                                  @Query("page") String page);

    @GET("movie/top_rated")
    Call<BaseMovieRespond> getRecommended(@Query("api_key") String apiKey,
                                          @Query("page") String page);

    @GET("movie/{id}")
    Call<MovieDetailRespond> getMovieDetail(@Path("id") String id,
                                            @Query("api_key") String apiKey);

    @GET("genre/movie/list")
    Call<GenreRespond> getGenre(@Query("api_key") String apiKey);

    @GET("genre/{genreId}/movies")
    Call<MovieInGenreRespond> getMovieInGenre(@Path("genreId") String genreId,
                                              @Query("api_key") String apiKey);

    @GET("search/movie")
    Call<BaseMovieRespond> getSearchMovie(@Query("api_key") String apiKey,
                                          @Query("query") String query,
                                          @Query("page") String page);

    @GET("movie/{movieId}/videos")
    Call<VideoMovieRespond> getMovieLink(@Path("movieId") String movieId,
                                         @Query("api_key") String apiKey);

    @GET("movie/{movieId}/credits")
    Call<MovieCreditRespond> getCredit(@Path("movieId") String movieId,
                                       @Query("api_key") String apiKey);
}
