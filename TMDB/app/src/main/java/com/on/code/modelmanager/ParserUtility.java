package com.on.code.modelmanager;

import android.util.Log;

import com.on.code.object.Movie;
import com.on.code.object.CatagoryObj;
import com.on.code.object.Chapter;
import com.on.code.object.CommentObj;
import com.on.code.object.Credits;
import com.on.code.object.MovieDetail;
import com.on.code.object.Quote;
import com.on.code.object.VideoLink;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ParserUtility {

    private final static String KEY_STATUS = "status";
    private final static String KEY_DATA = "results";
    private final static String KEY_SUCCESS = "SUCCESS";
    private final static String KEY_ERROR = "ERROR";

    private final static String ID = "id";
    private final static String TITLE = "title";
    private final static String AUTHOR = "author";
    private final static String PUBLISHER = "publisher";
    private final static String IMAGE = "image";
    private final static String DESCRIPTION = "description";
    private final static String LAST_CHAPTER = "last chapter";
    private final static String CHAPTER_ID = "chapter_id";
    private final static String BOOKID = "bookId";
    private final static String TITLE_CHAPTER = "title_chapter";
    private final static String BOOKMARKINDEX = "bookMarkIndex";
    private final static String ATTACHMENT = "attachment";
    private final static String TIME = "time";
    private final static String CHAPTERINDEX = "chapterIndex";
    private final static String ORDER = "order";
    private final static String STATUS = "status";
    private final static String CHAPTERID = "chapterId";
    private final static String NAME = "name";
    private final static String CONTENT = "content";
    private final static String DATETIME = "datetime";
    private final static String ALL_PAGE = "total_pages";
    private final static String QUOTEINDEX = "quoteIndex";

    // Movies
    private static final String POSTER_PATH = "poster_path";
    private static final String OVERVIEW = "overview";
    private static final String RELEASE_DATE = "release_date";
    private static final String KEY_POPULARITY = "popularity";
    private static final String VOTE_COUNT = "vote_count";
    private static final String VOTE_AVERAGE = "vote_average";
    private static final String GENRES = "genres";
    private static final String BACKDROP_PATH = "backdrop_path";
    private static final String LANGUAGE = "original_language";

    public static boolean isSuccessStatus(String json) {
        boolean isSuccess = false;
        try {
            JSONObject objJson = new JSONObject(json);
            isSuccess = objJson.getString(KEY_STATUS).equalsIgnoreCase(KEY_SUCCESS);
        } catch (JSONException e) {

        }
        return isSuccess;
    }

    // new
    public static ArrayList<Movie> parseListNewBooks(String json) {
        ArrayList<Movie> arr = new ArrayList<Movie>();
        try {
            JSONObject obj = new JSONObject(json);
            JSONArray arrJson, arrGenres;
            JSONObject bookJson = null, chapterJson = null;
            Movie movie = null;
            arrJson = obj.getJSONArray(KEY_DATA);
            List<String> ids;
            if (arrJson != null) {
                for (int i = 0; i < arrJson.length(); i++) {
                    bookJson = arrJson.getJSONObject(i);
                    movie = new Movie();
                    movie.setId(bookJson.getString(ID));
                    movie.setTitle(bookJson.getString(TITLE));
                    // movie.setAuthor(bookJson.getString(AUTHOR));
                    //movie.setPublisher(bookJson.getString(PUBLISHER));
                    movie.setImage(bookJson.getString(POSTER_PATH));
                    movie.setDesc(bookJson.getString(OVERVIEW));
                    movie.setReleaseDate(bookJson.getString(RELEASE_DATE));
                    movie.setVoteAverage(bookJson.getDouble(VOTE_AVERAGE));
                    movie.setVote(bookJson.getInt(VOTE_COUNT));
                    movie.setBackdropPath(bookJson.getString(BACKDROP_PATH));
                    movie.setLang(bookJson.getString(LANGUAGE));

                    arrGenres = bookJson.getJSONArray("genre_ids");
                    if (arrGenres != null) {
                        ids = new ArrayList<>();
                        for (int j = 0; j < arrGenres.length(); j++) {
                            ids.add(arrGenres.getString(j));
                        }
                        movie.setGenreIds(ids);
                    }

//                    movie.setDirect(chapterJson.getString("directURL"));
                    arr.add(movie);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return arr;
    }

    public static ArrayList<CatagoryObj> parseListCategory(String json) {
        ArrayList<CatagoryObj> arr = new ArrayList<CatagoryObj>();
        try {
            JSONObject obj = new JSONObject(json);
            JSONArray arrJson = null;
            JSONObject categoryJson = null;
            CatagoryObj category = null;
            arrJson = obj.getJSONArray(GENRES);
            if (arrJson != null) {
                for (int i = 0; i < arrJson.length(); i++) {
                    categoryJson = arrJson.getJSONObject(i);
                    category = new CatagoryObj();
                    category.setId(categoryJson.getInt(ID));
                    category.setTitle(categoryJson.getString(NAME));
                    arr.add(category);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return arr;
    }

    // new
    public static List<VideoLink> parseListLink(String json) {
        ArrayList<VideoLink> arr = new ArrayList<>();
        try {
            JSONObject obj = new JSONObject(json);
            JSONArray arrJson = null;
            JSONObject bookJson = null;
            VideoLink book = null;
            arrJson = obj.getJSONArray(KEY_DATA);
            if (arrJson != null) {
                for (int i = 0; i < arrJson.length(); i++) {
                    bookJson = arrJson.getJSONObject(i);
                    book = new VideoLink();

                    book.setId(bookJson.getString(ID));
                    book.setKey(bookJson.getString("key"));
                    book.setName(bookJson.getString(NAME));
                    book.setSite(bookJson.getString("site"));
                    book.setType(bookJson.getString("type"));

                    arr.add(book);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return arr;
    }

    public static MovieDetail getBookDetail(String json) {
        MovieDetail movieDetail = new MovieDetail();
        try {
            JSONObject obj = new JSONObject(json);
            movieDetail.setId(obj.getInt("id"));
            movieDetail.setTitle(obj.getString("title"));
            movieDetail.setOverview(obj.getString(OVERVIEW));
            movieDetail.setVote_average(obj.getInt("vote_average"));
            movieDetail.setRelease_date(obj.getString(RELEASE_DATE));

            JSONArray jsonArray = obj.getJSONArray("genres");
            JSONObject genresObj = null;
            List<MovieDetail.Genres> arrGenres = new ArrayList<>();
            MovieDetail.Genres genres;

            for (int i = 0; i < jsonArray.length(); i++) {
                genresObj = jsonArray.getJSONObject(i);
                genres = new MovieDetail.Genres();
                genres.setName(genresObj.getString("name"));

                arrGenres.add(genres);
            }
            movieDetail.setGenres(arrGenres);


        } catch (Exception e) {
            e.printStackTrace();
        }

        return movieDetail;
    }

    public static Credits getCredistDetail(String json) {
        Credits credits = new Credits();

        ArrayList<Credits.Cast> arrCast = new ArrayList<>();
        ArrayList<Credits.Crew> arrCrew = new ArrayList<>();

        Credits.Cast cast;
        Credits.Crew crew;

        try {
            JSONObject obj = new JSONObject(json);

            JSONArray arrJsoncast = null;
            JSONArray arrJsoncrew = null;

            JSONObject castJson = null;
            JSONObject crewJson = null;

            arrJsoncast = obj.getJSONArray("cast");

            for (int i = 0; i < arrJsoncast.length(); i++) {
                castJson = arrJsoncast.getJSONObject(i);
                cast = new Credits.Cast();
                cast.setCharacter(castJson.getString("name"));
                arrCast.add(cast);
            }

            credits.setCast(arrCast);

            arrJsoncrew = obj.getJSONArray("crew");

            for (int i = 0; i < arrJsoncrew.length(); i++) {
                crewJson = arrJsoncrew.getJSONObject(i);
                crew = new Credits.Crew();
                crew.setName(crewJson.getString("name"));
                arrCrew.add(crew);

            }
            credits.setCrew(arrCrew);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return credits;
    }

    public static ArrayList<CommentObj> parseListComment(String json) {
        ArrayList<CommentObj> arr = new ArrayList<CommentObj>();
        try {
            JSONObject obj = new JSONObject(json);
            JSONArray arrJson = null;
            JSONObject commentJson = null;
            CommentObj comment = null;
            if (KEY_SUCCESS.equalsIgnoreCase(obj.getString(KEY_STATUS))) {
                arrJson = obj.getJSONArray(KEY_DATA);
                for (int i = 0; i < arrJson.length(); i++) {
                    commentJson = arrJson.getJSONObject(i);
                    comment = new CommentObj();
                    comment.setId(commentJson.getInt(ID));
                    comment.setChapterId(commentJson.getInt(CHAPTERID));
                    comment.setBookId(commentJson.getInt(BOOKID));
                    comment.setName(commentJson.getString(NAME));
                    comment.setContent(commentJson.getString(CONTENT));
                    comment.setDatetime(commentJson.getString(DATETIME));
                    comment.setStatus(commentJson.getInt(STATUS));
                    arr.add(comment);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return arr;
    }

    public static int getTotalBookPage(String json) {
        try {
            JSONObject obj = new JSONObject(json);
            return obj.getInt(ALL_PAGE);
        } catch (JSONException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static ArrayList<Chapter> parseListChapters(String json) {
        ArrayList<Chapter> arr = new ArrayList<Chapter>();
        try {
            JSONObject obj = new JSONObject(json);
            JSONArray arrJson = null;
            JSONObject bookJson = null;
            Chapter chapter = null;
            if (KEY_SUCCESS.equalsIgnoreCase(obj.getString(KEY_STATUS))) {
                arrJson = obj.getJSONArray(KEY_DATA);
                for (int i = 0; i < arrJson.length(); i++) {
                    bookJson = arrJson.getJSONObject(i);
                    chapter = new Chapter();
                    chapter.setId(bookJson.getString(ID));
                    chapter.setBookId(bookJson.getString(BOOKID));
                    chapter.setTitle(bookJson.getString(TITLE));
                    chapter.setCurrentBookMarkIndex(bookJson
                            .getString(BOOKMARKINDEX));
//					chapter.setType(bookJson.getInt("type"));
                    chapter.setAttachedFile(bookJson.getString(ATTACHMENT));
//                    chapter.setDirectUrl(bookJson.getString("directURL"));
                    arr.add(chapter);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return arr;
    }

    public static ArrayList<Quote> parseListQuotes(String json) {
        ArrayList<Quote> arr = new ArrayList<Quote>();
        try {
            JSONObject obj = new JSONObject(json);
            JSONArray arrJson = null;
            JSONObject bookJson = null;
            Quote quote = null;
            if (KEY_SUCCESS.equalsIgnoreCase(obj.getString(KEY_STATUS))) {
                arrJson = obj.getJSONArray(KEY_DATA);
                for (int i = 0; i < arrJson.length(); i++) {
                    bookJson = arrJson.getJSONObject(i);
                    quote = new Quote();
                    quote.setId(bookJson.getString(ID));
                    quote.setQuoteIndex(bookJson.getString(QUOTEINDEX));
                    quote.setContent(bookJson.getString(CONTENT));
                    quote.setChapterId(bookJson.getString(CHAPTERID));
                    quote.setImage(bookJson.getString(IMAGE));
                    arr.add(quote);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("parser", e.getMessage());
        }
        return arr;
    }

    // ==========================================================================================================

    private static boolean getBooleanValue(JSONObject obj, String name) {
        try {
            return obj.getBoolean(name);
        } catch (Exception e) {
            return false;
        }
    }

    private static String getStringValue(JSONObject obj, String key) {
        try {
            return obj.isNull(key) ? "" : obj.getString(key);
        } catch (JSONException e) {
            return "";
        }
    }

    private static long getLongValue(JSONObject obj, String key) {
        try {
            return obj.isNull(key) ? 0L : obj.getLong(key);
        } catch (JSONException e) {
            return 0L;
        }
    }

    private static int getIntValue(JSONObject obj, String key) {
        try {
            return obj.isNull(key) ? 0 : obj.getInt(key);
        } catch (JSONException e) {
            return 0;
        }
    }

    private static Double getDoubleValue(JSONObject obj, String key) {
        double d = 0.0;
        try {
            return obj.isNull(key) ? d : obj.getDouble(key);
        } catch (JSONException e) {
            return d;
        }
    }

    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    public static boolean isJsonObject(JSONObject parent, String key) {
        try {
            JSONObject jObj = parent.getJSONObject(key);
        } catch (JSONException e) {
            return false;
        }
        return true;
    }

    public static boolean isDouble(String string) {
        try {
            Double.parseDouble(string);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

}
