package com.on.code.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.on.code.BaseFragment;
import com.on.code.R;
import com.on.code.activity.MainSherkLockActivity;
import com.on.code.config.GlobalValue;
import com.on.code.util.FileUtility;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@SuppressLint("NewApi")
public class SettingFragment extends BaseFragment implements OnClickListener {

    private TextView lblTitle;
    private static Activity self;
    private ImageView btnTopMenu;
    private final int MIN_FONT = 15;
    private final int MAX_FONT = 50;
    private Button btnApply, btnClearCache;
    private SeekBar seekBarFont;
    private Spinner spnFont;
    private TextView lblSample;
    private List<String> listFont;
    private String[] arrFont;
    private int indexFont;
    private CheckBox chkScreenOn;
    private Dialog dialog;

    public static SettingFragment newInstance() {

        Bundle args = new Bundle();

        SettingFragment fragment = new SettingFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting, container,
                false);
        self = getActivity();
        initUI(view);
        initControl();
        initSetting();
        getListFontToSpinner();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initUI(View view) {
        lblTitle = (TextView) view.findViewById(R.id.lblTopTitle);
        lblTitle.setText(getResources().getString(R.string.menuSetting));
        btnTopMenu = (ImageView) view.findViewById(R.id.btnTopLeft);
        btnApply = (Button) view.findViewById(R.id.btnApply);
        btnClearCache = (Button) view.findViewById(R.id.btnClearCache);
        seekBarFont = (SeekBar) view.findViewById(R.id.seekBarFont);
        spnFont = (Spinner) view.findViewById(R.id.spnFont);
        lblSample = (TextView) view.findViewById(R.id.lblSample);
        chkScreenOn = (CheckBox) view.findViewById(R.id.chkScreenOn);

    }

    private void initControl() {

        btnApply.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                GlobalValue.preferences.putFontSize(getFontSize(seekBarFont
                        .getProgress()));
                GlobalValue.preferences.putFont("fonts/" + arrFont[indexFont]);
                GlobalValue.createTypeFace(self.getBaseContext());
                GlobalValue.preferences.setScreenOn(chkScreenOn.isChecked());
                ((MainSherkLockActivity) self).updateScreenOn();
                Toast.makeText(self, "Update font style successfully !",
                        Toast.LENGTH_SHORT).show();

            }
        });

        //
        btnClearCache.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                dialog = new Dialog(getActivity());
                dialog.setTitle("Clear Cache");
                dialog.setContentView(R.layout.dialog_clear_cache);
                final CheckBox cbClearEpub = (CheckBox) dialog
                        .findViewById(R.id.cbClearEpub);
                final CheckBox cbClearPDF = (CheckBox) dialog
                        .findViewById(R.id.cbClearPDF);
                final Button btnCancel = (Button) dialog
                        .findViewById(R.id.btnCancel);
                final Button btnOk = (Button) dialog.findViewById(R.id.btnOK);

                btnOk.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                        if (cbClearEpub.isChecked() && cbClearPDF.isChecked()) {
                            if (FileUtility.deleteFilesInFolder(GlobalValue.CACHE_FOLDER, "")) {
                                Toast.makeText(self, "Deleting all data cache successfully!",
                                        Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(self, "Deleting Cache false. Try again please!",
                                        Toast.LENGTH_SHORT).show();
                            }
                        } else if (cbClearPDF.isChecked()) {
                            if (FileUtility.deleteFilesInFolder(GlobalValue.CACHE_FOLDER, ".pdf")) {
                                Toast.makeText(self, "Deleting PDF data cache successfully!",
                                        Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(self, "Deleting PDF Cache failed. Try again please!",
                                        Toast.LENGTH_SHORT).show();
                            }
                        } else if (cbClearEpub.isChecked()) {
                            if (FileUtility.deleteFilesInFolder(GlobalValue.CACHE_FOLDER, ".epub")) {
                                Toast.makeText(self, "Deleting EPUB data cache successfully!",
                                        Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(self, "Deleting EPUB Cache false. Try again please!",
                                        Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(self, "Please select option..!",
                                    Toast.LENGTH_SHORT).show();
                        }

                    }

                });

                btnCancel.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });

                dialog.show();

            }
        });
        spnFont.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> av, View v, int position,
                                       long l) {
                indexFont = position;
                GlobalValue.setTypeFace(self.getBaseContext(), "fonts/"
                                + arrFont[position],
                        getFontSize(seekBarFont.getProgress()), lblSample);
            }

            @Override
            public void onNothingSelected(AdapterView<?> av) {
            }
        });

        seekBarFont.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                lblSample.setTextSize(getFontSize(progress));
            }
        });
    }

    public static boolean deleteDirectory(File path) {
        // TODO Auto-generated method stub
        if (path.exists()) {
            File[] files = path.listFiles();
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    deleteDirectory(files[i]);
                } else {
                    files[i].delete();
                }
            }
        }
        return (path.delete());
    }

    private void initSetting() {
        if (GlobalValue.preferences == null) {
            seekBarFont.setProgress(MIN_FONT);
        } else {
            seekBarFont.setProgress(getProgress(GlobalValue.preferences
                    .getFontSize()));
            lblSample.setTextSize(GlobalValue.preferences.getFontSize());
        }
        chkScreenOn.setChecked(GlobalValue.preferences.isScreenOn());
        GlobalValue.setTypeFace(lblSample);
        getListFontToSpinner();

    }

    private void getListFontToSpinner() {
        try {
            listFont = new ArrayList<String>();
            arrFont = self.getAssets().list("fonts");
            for (String name : arrFont) {
                listFont.add(name.substring(0, name.length() - 4));
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(self,
                    R.layout.spiner_item, listFont);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spnFont.setAdapter(adapter);
            spnFont.setSelection(getIndexFont());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private int getIndexFont() {
        for (int i = 0; i < listFont.size(); i++) {
            if (GlobalValue.preferences != null) {
                String font = GlobalValue.preferences.getFont().substring(6,
                        GlobalValue.preferences.getFont().length() - 4);
                if (font.equals(listFont.get(i))) {
                    indexFont = i;
                    return i;
                }
            }
        }
        indexFont = 0;
        return 0;
    }

    private float getFontSize(int progress) {
        return (MIN_FONT + (MAX_FONT - MIN_FONT) * (((float) progress) / 100));
    }

    private int getProgress(float fontSize) {
        return (int) (100 * (fontSize - MIN_FONT) / (MAX_FONT - MIN_FONT));
    }

    @Override
    public void onClick(View v) {

    }

}
