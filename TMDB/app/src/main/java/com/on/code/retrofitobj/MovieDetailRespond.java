package com.on.code.retrofitobj;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MovieDetailRespond {

    @SerializedName("crew")
    private List<Crew> crew;
    @SerializedName("cast")
    private List<Cast> cast;
    @SerializedName("id")
    private int id;

    public List<Crew> getCrew() {
        return crew;
    }

    public void setCrew(List<Crew> crew) {
        this.crew = crew;
    }

    public List<Cast> getCast() {
        return cast;
    }

    public void setCast(List<Cast> cast) {
        this.cast = cast;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public static class Crew {
        @SerializedName("profile_path")
        private String profile_path;
        @SerializedName("name")
        private String name;
        @SerializedName("job")
        private String job;
        @SerializedName("id")
        private int id;
        @SerializedName("gender")
        private int gender;
        @SerializedName("department")
        private String department;
        @SerializedName("credit_id")
        private String credit_id;

        public String getProfile_path() {
            return profile_path;
        }

        public void setProfile_path(String profile_path) {
            this.profile_path = profile_path;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getJob() {
            return job;
        }

        public void setJob(String job) {
            this.job = job;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getGender() {
            return gender;
        }

        public void setGender(int gender) {
            this.gender = gender;
        }

        public String getDepartment() {
            return department;
        }

        public void setDepartment(String department) {
            this.department = department;
        }

        public String getCredit_id() {
            return credit_id;
        }

        public void setCredit_id(String credit_id) {
            this.credit_id = credit_id;
        }
    }

    public static class Cast {
        @SerializedName("profile_path")
        private String profile_path;
        @SerializedName("order")
        private int order;
        @SerializedName("name")
        private String name;
        @SerializedName("id")
        private int id;
        @SerializedName("gender")
        private int gender;
        @SerializedName("credit_id")
        private String credit_id;
        @SerializedName("character")
        private String character;
        @SerializedName("cast_id")
        private int cast_id;

        public String getProfile_path() {
            return profile_path;
        }

        public void setProfile_path(String profile_path) {
            this.profile_path = profile_path;
        }

        public int getOrder() {
            return order;
        }

        public void setOrder(int order) {
            this.order = order;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getGender() {
            return gender;
        }

        public void setGender(int gender) {
            this.gender = gender;
        }

        public String getCredit_id() {
            return credit_id;
        }

        public void setCredit_id(String credit_id) {
            this.credit_id = credit_id;
        }

        public String getCharacter() {
            return character;
        }

        public void setCharacter(String character) {
            this.character = character;
        }

        public int getCast_id() {
            return cast_id;
        }

        public void setCast_id(int cast_id) {
            this.cast_id = cast_id;
        }
    }
}
