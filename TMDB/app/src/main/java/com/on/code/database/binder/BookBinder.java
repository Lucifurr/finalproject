package com.on.code.database.binder;

import android.database.sqlite.SQLiteStatement;

import com.on.code.database.ParameterBinder;
import com.on.code.object.Movie;

public class BookBinder implements ParameterBinder {

	@Override
	public void bind(SQLiteStatement st, Object object) {
		// TODO Auto-generated method stub
		Movie movieObj = (Movie) object;
		st.bindString(1, movieObj.getId());
		st.bindString(2, movieObj.getTitle());
		st.bindString(3, movieObj.getReleaseDate());
		st.bindString(4, String.valueOf(movieObj.getVoteAverage()));
		st.bindString(5, movieObj.getImage());
		st.bindString(6, movieObj.getDesc());
		st.bindString(7, "");
		st.bindString(6, "");

	}
}
