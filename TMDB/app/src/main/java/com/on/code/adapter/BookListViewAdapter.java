package com.on.code.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.on.code.R;
import com.on.code.config.GlobalValue;
import com.on.code.object.Movie;
import com.squareup.picasso.Picasso;

public class BookListViewAdapter extends BaseAdapter {
    private Activity mainActivity;
    private List<Movie> listVerses;
    private LayoutInflater layoutInflater;
    private AQuery aq;
    public boolean isHistory = false;

    private IOnClickItemListener listener;

    public interface IOnClickItemListener{
        void onItemClicked(int position);
    }

    public BookListViewAdapter(Activity activity,
                               ArrayList<Movie> listverseVerses, IOnClickItemListener listener) {
        this.mainActivity = activity;
        this.listener = listener;
        this.listVerses = listverseVerses;
        this.layoutInflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        aq = new AQuery(mainActivity);

    }

    public int getCount() {
        return listVerses.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        // TODO Auto-generated method stub
        final Hoder holder;
        if (convertView == null) {
            holder = new Hoder();
            convertView = layoutInflater.inflate(R.layout.item_list_main, null);
            holder.lblChapterTitle = (TextView) convertView
                    .findViewById(R.id.lblChapterTitle);
            holder.lblChapterTitle.setSelected(true);
            holder.lblBookTitle = (TextView) convertView
                    .findViewById(R.id.lblBookTitle);
            holder.imgBook = (ImageView) convertView
                    .findViewById(R.id.imgBookIcon);
            holder.lblDesc = (TextView) convertView.findViewById(R.id.lblDesc);
            holder.lblYear = (TextView) convertView
                    .findViewById(R.id.lblYear);
            holder.rbVote = (RatingBar) convertView.findViewById(R.id.rbVoting);
            convertView.setTag(holder);

        } else {
            holder = (Hoder) convertView.getTag();
        }

        final Movie movie = listVerses.get(position);
        if (movie != null) {
            if (GlobalValue.preferences != null) {
                GlobalValue.setTypeFont(holder.lblBookTitle, holder.lblDesc,
                        holder.lblChapterTitle, holder.lblYear
                );
            }
            holder.lblChapterTitle.setText(movie.getTitle());
            String year = "";
            if (movie.getReleaseDate() != null && movie.getReleaseDate().length()>5){
                year = movie.getReleaseDate().substring(0,4);
            }
            holder.lblYear.setText(year);
            holder.rbVote.setRating((float) movie.getVoteAverage() / 2);

            //DrawableCompat.setTint(holder.rbVote.getProgressDrawable(), Color.YELLOW);
            //holder.rbVote.getProgressDrawable().setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);

            LayerDrawable stars = (LayerDrawable) holder.rbVote.getProgressDrawable();
            stars.getDrawable(2).setColorFilter(mainActivity.getResources().getColor(R.color.all_color), PorterDuff.Mode.SRC_ATOP);

            holder.lblBookTitle.setText(GlobalValue.genresIdsToString(movie.getGenreIds()));
            holder.lblDesc.setText(movie.getDesc());
            // holder.lblChaplast.setText(movie.getTitleChapter());
            if (!isHistory) {
                Picasso.with(mainActivity).load(movie.getImage()).into(holder.imgBook);
            }else{
                Picasso.with(mainActivity).load(movie.getOriginalImage()).into(holder.imgBook);
            }

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClicked(position);
                }
            });

        }

        return convertView;


    }


    static class Hoder {
        RatingBar rbVote;
        TextView lblChapterTitle, lblBookTitle, lblDesc, lblYear;
        ImageView imgBook;
    }
}