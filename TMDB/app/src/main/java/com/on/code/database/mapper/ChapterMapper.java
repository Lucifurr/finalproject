package com.on.code.database.mapper;

import android.annotation.SuppressLint;
import android.database.Cursor;

import com.on.code.database.CursorParseUtility;
import com.on.code.database.IRowMapper;
import com.on.code.object.Chapter;

@SuppressLint("NewApi")
public class ChapterMapper implements IRowMapper<Chapter> {
	@Override
	public Chapter mapRow(Cursor row, int rowNum) {
		Chapter chapter = new Chapter();
		chapter.setId(CursorParseUtility.getString(row, "id"));
		chapter.setBookId(CursorParseUtility.getString(row, "bookId"));
		chapter.setTitle(CursorParseUtility.getString(row, "title"));
		String bookmark = CursorParseUtility.getString(row, "bookMarkIndex");
		if (bookmark.isEmpty()) {
			bookmark = "0,0";
		}
		chapter.setCurrentBookMarkIndex(bookmark);
		return chapter;
	}
}
