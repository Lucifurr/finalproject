package com.on.code.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.on.code.R;
import com.on.code.config.GlobalValue;
import com.on.code.object.Chapter;

public class ChapterAdapter extends BaseAdapter {
	private String TAG = "CategoryAdapter.java";
	private List<Chapter> listBaptismalInfo;
	private LayoutInflater inflater = null;
	private Activity act;
	private int currentIndex = -1;

	public ChapterAdapter(Activity activity, List<Chapter> listCategoryInfo) {
		this.listBaptismalInfo = listCategoryInfo;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		act = activity;
	}

	public void setCurrentIndex(int index) {
		currentIndex = index;
	}

	public int getCount() {
		return listBaptismalInfo.size();
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.row_list_chapter, null);
			holder = new ViewHolder();
			holder.layoutRowChapter = (LinearLayout) convertView
					.findViewById(R.id.layoutRowChapter);
			holder.lblId = (TextView) convertView.findViewById(R.id.lblId);
			holder.lblTitle = (TextView) convertView
					.findViewById(R.id.lblTitle);

			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		// set data at here
		Chapter item = listBaptismalInfo.get(position);
		if (item != null) {
			// holder = (ViewHolder) convertView.getTag();
			if (GlobalValue.preferences != null) {
				GlobalValue.setTypeFont(holder.lblTitle, holder.lblId);
			}
			if (position == currentIndex) {
				holder.layoutRowChapter.setBackgroundResource(R.color.blue_header);
			} else {
				holder.layoutRowChapter
						.setBackgroundResource(R.color.transparent);
			}
			holder.lblTitle.setText(item.getTitle());
			holder.lblId.setText((position + 1) + "");
		} else {
			Log.i(TAG, "Null Object !");
		}

		return convertView;
	}

	private class ViewHolder {
		private TextView lblId;
		private TextView lblTitle;
		private LinearLayout layoutRowChapter;
	}
}
