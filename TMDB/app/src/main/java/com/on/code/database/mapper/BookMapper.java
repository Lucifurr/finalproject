package com.on.code.database.mapper;

import android.database.Cursor;

import com.on.code.database.CursorParseUtility;
import com.on.code.database.IRowMapper;
import com.on.code.object.Movie;

public class BookMapper implements IRowMapper<Movie> {
	@Override
	public Movie mapRow(Cursor row, int rowNum) {
		Movie movie = new Movie();
		movie.setId(CursorParseUtility.getString(row, "bookId"));
		movie.setTitle(CursorParseUtility.getString(row, "title"));
		movie.setAuthor(CursorParseUtility.getString(row, "author"));
		movie.setVoteAverage(Double.parseDouble(CursorParseUtility.getString(row, "publisher")));
		movie.setImage(CursorParseUtility.getString(row, "image"));
		movie.setDesc(CursorParseUtility.getString(row, "description"));
		movie.setChapLast(CursorParseUtility.getString(row, "last chapter"));
		return movie;
	}
}
