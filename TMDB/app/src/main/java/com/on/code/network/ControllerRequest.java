package com.on.code.network;

import android.app.Application;


public class ControllerRequest extends Application {
    public static final String TAG = ControllerRequest.class.getSimpleName();
    private static ControllerRequest controller;

    @Override
    public void onCreate() {
        super.onCreate();
        controller = this;
    }

    /**
     * @return
     */

    public static ControllerRequest getInstance() {
        return controller;
    }

}
