package com.on.code.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.internal.view.ContextThemeWrapper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.on.code.BaseFragment;
import com.on.code.R;
import com.on.code.adapter.HistoryAdapter;
import com.on.code.database.DatabaseUtility;
import com.on.code.object.Movie;

import java.util.ArrayList;

public class HistoryFragment extends BaseFragment {

    private View v;
    //    private BookListViewAdapter lsv_adapter;
    private HistoryAdapter lsv_adapter;
    private ArrayList<Movie> mArrHistory;
    private ListView lsvHistory;
    private Button btnClearHistory;
    private TextView lblNoData;

    public static HistoryFragment newInstance() {

        Bundle args = new Bundle();

        HistoryFragment fragment = new HistoryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        v = inflater.inflate(R.layout.fragment_history, container, false);
        initUI();
        initControl();
        initListView();
        return v;
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        setData();
    }

    private void initUI() {
        lsvHistory = (ListView) v.findViewById(R.id.lsvHistory);
        btnClearHistory = (Button) v.findViewById(R.id.btnClear);
        lblNoData = (TextView) v.findViewById(R.id.lblNoData);
        btnClearHistory.setVisibility(View.GONE);
    }

    private void initControl() {

        // clear history

        btnClearHistory.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO Auto-generated method stub
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        new ContextThemeWrapper(getActivity(), android.R.style.Theme_Holo_Light_Dialog));
                builder.setTitle("Clear History");
                builder.setMessage("Are you sure want to clear?");
                builder.setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();
                            }
                        });
                builder.setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                DatabaseUtility.clearHistory(getActivity());
                                lsvHistory.setAdapter(null);
                                lblNoData.setVisibility(View.VISIBLE);
                                btnClearHistory.setVisibility(View.GONE);
                                initListView();
                                Toast.makeText(getActivity(), "Successfully!",
                                        Toast.LENGTH_SHORT).show();
                                dialog.dismiss();

                            }
                        });
                builder.create().show();
            }
        });

    }

    private void initListView() {
        if (mArrHistory != null) {
            mArrHistory = new ArrayList<>();
//            lsv_adapter = new BookListViewAdapter(getActivity(), mArrHistory, new BookListViewAdapter.IOnClickItemListener() {
//                @Override
//                public void onItemClicked(int position) {
//                    GlobalValue.movie = mArrHistory.get(position);
//                    GlobalValue.imagebook = mArrHistory.get(position).getImage();
//                    Bundle b = new Bundle();
//                    b.putParcelable(DetailActivity.KEY_OBJECT, mArrHistory.get(position));
//
//                    startActivity(DetailActivity.class, b);
//                }
//            });
            lsv_adapter = new HistoryAdapter(getActivity(), mArrHistory);

            lsv_adapter.isHistory = true;
            lsvHistory.setAdapter(lsv_adapter);
        }
    }

    private void setData() {
        ArrayList<Movie> arr = DatabaseUtility.getListHistory(getActivity());
        if (mArrHistory != null) {
            mArrHistory.clear();
            mArrHistory.addAll(arr);
            lsv_adapter.notifyDataSetChanged();
            if (mArrHistory.size() <= 0) {
                Log.e("History", "null");
                lblNoData.setVisibility(View.VISIBLE);
                btnClearHistory.setVisibility(View.GONE);
            } else {
                lblNoData.setVisibility(View.GONE);
                btnClearHistory.setVisibility(View.VISIBLE);
            }
        } else {
            mArrHistory = arr;
            lsv_adapter = new HistoryAdapter(getActivity(), mArrHistory);
            lsvHistory.setAdapter(lsv_adapter);
            if (mArrHistory.size() <= 0) {
                Log.e("History", "null");
                btnClearHistory.setVisibility(View.GONE);
                lblNoData.setVisibility(View.VISIBLE);
            } else {
                lblNoData.setVisibility(View.GONE);
                btnClearHistory.setVisibility(View.VISIBLE);
            }
        }
    }

    private void startActivity(Class<?> cls, Bundle bundle) {
        Intent intent = new Intent(getActivity(), cls);
        intent.putExtras(bundle);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_in_left,
                R.anim.slide_out_left);
    }


}
