package com.on.code.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class FileUtility {

    public static List<File> getListFiles(String folderUrl, String extensionName) {

        List<File> res = new ArrayList<File>();
        File folderFile = new File(folderUrl);
        if (folderFile.isDirectory()) {
            File[] f = folderFile.listFiles();
            if (f != null) {
                for (int i = 0; i < f.length; i++) {
                    if (f[i].isDirectory()) {
                        res.addAll(getListFiles(f[i].getAbsolutePath(), extensionName));
                    } else {
                        String lowerCasedName = f[i].getName().toLowerCase();
                        if (lowerCasedName.endsWith(extensionName)) {
                            res.add(f[i]);
                        }
                    }
                }
            }
        }
        return res;
    }

    public static boolean deleteFilesInFolder(String folderUrl, String extensionName) {
        File folderFile = new File(folderUrl);
        try {
            if (folderFile.exists()) {
                if (folderFile.isDirectory()) {
                    File[] f = folderFile.listFiles();
                    if (f != null) {
                        for (int i = 0; i < f.length; i++) {
                            if (f[i].isDirectory()) {
                                deleteFilesInFolder(f[i].getAbsolutePath(), extensionName);
                            } else {
                                if (extensionName.isEmpty()) {
                                    f[i].deleteOnExit();
                                } else {
                                    String lowerCasedName = f[i].getName().toLowerCase();
                                    if (lowerCasedName.endsWith(extensionName)) {
                                        f[i].delete();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
