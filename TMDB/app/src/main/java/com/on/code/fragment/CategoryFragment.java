package com.on.code.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.on.code.BaseFragment;
import com.on.code.R;
import com.on.code.activity.MovieByCategoryActivity;
import com.on.code.adapter.CategoryAdapter;
import com.on.code.config.GlobalValue;
import com.on.code.object.CatagoryObj;

import java.util.ArrayList;

public class CategoryFragment extends BaseFragment {

    private View v;
    private GridView grvCategory;
    private ArrayList<CatagoryObj> arrCategory;
    private CategoryAdapter categoryAdapter;

    public static CategoryFragment newInstance() {

        Bundle args = new Bundle();

        CategoryFragment fragment = new CategoryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        arrCategory = GlobalValue.getCategoires();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        v = inflater.inflate(R.layout.fragment_category, container, false);
        initUI();
        initControl();

        return v;
    }

    private void initUI() {
        grvCategory = (GridView) v.findViewById(R.id.grvCategory);
    }

    private void initControl() {
        categoryAdapter = new CategoryAdapter(getActivity(), arrCategory);
        grvCategory.setAdapter(categoryAdapter);

        grvCategory.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                Bundle b = new Bundle();
                b.putString("categoryId", arrCategory.get(position).getId() + "");
                b.putString("cateTitle", arrCategory.get(position).getTitle());
                startActivity(MovieByCategoryActivity.class, b);
            }
        });
    }


    private void startActivity(Class<?> cls, Bundle bundle) {
        Intent intent = new Intent(getActivity(), cls);
        intent.putExtras(bundle);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_in_left,
                R.anim.slide_out_left);
    }

    private void startActivity(Class<?> cls) {
        Intent intent = new Intent(getActivity(), cls);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_in_left,
                R.anim.slide_out_left);
    }
}
