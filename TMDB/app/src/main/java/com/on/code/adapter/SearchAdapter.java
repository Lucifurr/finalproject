package com.on.code.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.on.code.R;
import com.on.code.object.Movie;

public class SearchAdapter extends BaseAdapter {
	private Activity mainActivity;
	private List<Movie> listVerses;
	private LayoutInflater layoutInflater;
	private AQuery aq;
	private boolean memCache = false;
	private boolean fileCache = true;

	public SearchAdapter(Activity activity, ArrayList<Movie> listverseVerses) {
		this.mainActivity = activity;
		this.listVerses = listverseVerses;
		this.layoutInflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public int getCount() {
		return listVerses.size();
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		// TODO Auto-generated method stub
		final Hoder holder;
		if (convertView == null) {
			holder = new Hoder();
			convertView = layoutInflater.inflate(R.layout.item_bookmark, null);
			holder.lblChapterTitle = (TextView) convertView
					.findViewById(R.id.lblChapterTitle);
			holder.lblBookTitle = (TextView) convertView
					.findViewById(R.id.lblBookTitle);
			holder.imgBook = (ImageView) convertView
					.findViewById(R.id.imgBookIcon);
			convertView.setTag(holder);

		} else {
			holder = (Hoder) convertView.getTag();
		}

		Movie chapter = listVerses.get(position);
		if (chapter != null) {
			aq = new AQuery(mainActivity);

			holder.lblChapterTitle.setText(chapter.getTitle());
			holder.lblBookTitle.setText(chapter.getTitle());
			aq.id(holder.imgBook)
					.image(chapter.getImage(), memCache, fileCache);
		}

		return convertView;
	}

	static class Hoder {
		TextView lblChapterTitle, lblBookTitle;
		ImageView imgBook;
	}
}
