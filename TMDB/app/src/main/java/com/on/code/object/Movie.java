package com.on.code.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.on.code.config.WebServiceConfig;

import java.util.List;

public class Movie implements Parcelable {
    @SerializedName("id")
	private String id;
    @SerializedName("release_date")
	private String releaseDate;
    @SerializedName("vote_count")
	private int vote;
    @SerializedName("vote_average")
	private double voteAverage;
    @SerializedName("title")
	private String title;
	private String author;
	private String publisher;
	private String image;
	@SerializedName("overview")
	private String desc;
	// chapter
	private String titleChapter, attachment, time;
	private int chapterId, bookId, typeChapter, chapterIndex,bookMark;
//    private String direct;
	private String chapLast;
	@SerializedName("backdrop_path")
	private String  backdropPath;
	@SerializedName("original_language")
	private String lang;
	private List<VideoLink>  links;
	private List<String> genreIds;
//
//    public String getDirect() {
//        return direct;
//    }
//
//    public void setDirect(String direct) {
//        this.direct = direct;
//    }


	public Movie(){}


	protected Movie(Parcel in) {
		id = in.readString();
		releaseDate = in.readString();
		vote = in.readInt();
		voteAverage = in.readDouble();
		title = in.readString();
		author = in.readString();
		publisher = in.readString();
		image = in.readString();
		desc = in.readString();
		titleChapter = in.readString();
		attachment = in.readString();
		time = in.readString();
		chapterId = in.readInt();
		bookId = in.readInt();
		typeChapter = in.readInt();
		chapterIndex = in.readInt();
		bookMark = in.readInt();
		chapLast = in.readString();
		backdropPath = in.readString();
		lang = in.readString();
		genreIds = in.createStringArrayList();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(id);
		dest.writeString(releaseDate);
		dest.writeInt(vote);
		dest.writeDouble(voteAverage);
		dest.writeString(title);
		dest.writeString(author);
		dest.writeString(publisher);
		dest.writeString(image);
		dest.writeString(desc);
		dest.writeString(titleChapter);
		dest.writeString(attachment);
		dest.writeString(time);
		dest.writeInt(chapterId);
		dest.writeInt(bookId);
		dest.writeInt(typeChapter);
		dest.writeInt(chapterIndex);
		dest.writeInt(bookMark);
		dest.writeString(chapLast);
		dest.writeString(backdropPath);
		dest.writeString(lang);
		dest.writeStringList(genreIds);
	}

	@Override
	public int describeContents() {
		return 0;
	}

	public static final Creator<Movie> CREATOR = new Creator<Movie>() {
		@Override
		public Movie createFromParcel(Parcel in) {
			return new Movie(in);
		}

		@Override
		public Movie[] newArray(int size) {
			return new Movie[size];
		}
	};

	public List<String> getGenreIds() {
		return genreIds;
	}

	public void setGenreIds(List<String> genreIds) {
		this.genreIds = genreIds;
	}

	public String getChapLast() {
		return chapLast;
	}

	public void setChapLast(String chapLast) {
		this.chapLast = chapLast;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getImage() {
		return WebServiceConfig.getImageLink("w300",image);
	}

	public String getOriginalImage(){
		return image;
	}

	public String getImage(String size) {
		return WebServiceConfig.getImageLink(size,image);
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	//


	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getTitleChapter() {
		return titleChapter;
	}

	public void setTitleChapter(String titleChapter) {
		this.titleChapter = titleChapter;
	}

	public int getBookMark() {
		return bookMark;
	}

	public void setBookMark(int bookMark) {
		this.bookMark = bookMark;
	}

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public int getChapterId() {
		return chapterId;
	}

	public void setChapterId(int chapterId) {
		this.chapterId = chapterId;
	}

	public int getBookId() {
		return bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	public int getTypeChapter() {
		return typeChapter;
	}

	public void setTypeChapter(int typeChapter) {
		this.typeChapter = typeChapter;
	}

	public int getChapterIndex() {
		return chapterIndex;
	}

	public void setChapterIndex(int chapterIndex) {
		this.chapterIndex = chapterIndex;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public double getVoteAverage() {
		return voteAverage;
	}

	public void setVoteAverage(double voteAverage) {
		this.voteAverage = voteAverage;
	}

	public int getVote() {
		return vote;
	}

	public void setVote(int vote) {
		this.vote = vote;
	}

	public List<VideoLink> getLinks() {
		return links;
	}

	public void setLinks(List<VideoLink> links) {
		this.links = links;
	}

	public String getBackdropPath() {
		return backdropPath;
	}

	public void setBackdropPath(String backdropPath) {
		this.backdropPath = backdropPath;
	}
}
