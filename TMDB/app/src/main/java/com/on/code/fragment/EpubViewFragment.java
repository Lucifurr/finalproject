package com.on.code.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.on.code.BaseFragment;
import com.on.code.R;
import com.on.code.config.GlobalValue;
import com.on.code.epub.EpubNavigator;
import com.on.code.epub.SplitPanel;
import com.on.code.object.Chapter;
import com.on.code.util.DialogUtility;
import com.on.code.util.DownloadFileAsync;

public class EpubViewFragment extends BaseFragment {

    private Activity self;
    public EpubNavigator navigator;
    protected int bookSelector;
    protected int panelCount;
    public Chapter chapter;
    View view;

    public static EpubViewFragment getInstances(Chapter kind) {
        EpubViewFragment frag = new EpubViewFragment();
        frag.chapter = kind;
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_epub_view, container, false);
        self = getActivity();
        navigator = new EpubNavigator(2, this);
        panelCount = 0;

        return view;
    }

    private void initData() {
        if (!chapter.isCacheAttachedFile()) {
            new DownloadFileAsync(self, chapter.getAttachedFile(), GlobalValue.CACHE_FOLDER, new DownloadFileAsync.DownloadListener() {
                @Override
                public void onSuccess() {
                    navigator.openBook(chapter.getCacheAttachedFileUrl(), bookSelector);
                }

                @Override
                public void onError() {
                    DialogUtility.alert(self, R.string.error_LoadPage);
                }
            }).execute();
        } else {
            navigator.openBook(chapter.getCacheAttachedFileUrl(), bookSelector);
        }
    }


    public void onResume() {
        super.onResume();
        if (panelCount == 0) {
            SharedPreferences preferences = getActivity().getPreferences(Activity.MODE_PRIVATE);
            navigator.loadViews(preferences);
        }
        initData();
    }

    @Override
    public void onPause() {
        super.onPause();
        SharedPreferences preferences = getActivity().getPreferences(Activity.MODE_PRIVATE);
        Editor editor = preferences.edit();
        saveState(editor);
        editor.commit();
    }

    // ---- Panels Manager
    public void addPanel(SplitPanel p) {
        FragmentTransaction fragmentTransaction = getChildFragmentManager()
                .beginTransaction();
        fragmentTransaction.add(R.id.MainLayout, p, p.getTag());
        fragmentTransaction.commit();

        panelCount++;
    }

    public void attachPanel(SplitPanel p) {
        FragmentTransaction fragmentTransaction = getChildFragmentManager()
                .beginTransaction();
        fragmentTransaction.attach(p);
        fragmentTransaction.commit();

        panelCount++;
    }

    public void detachPanel(SplitPanel p) {
        FragmentTransaction fragmentTransaction = getChildFragmentManager()
                .beginTransaction();
        fragmentTransaction.detach(p);
        fragmentTransaction.commit();

        panelCount--;
    }

    public void removePanelWithoutClosing(SplitPanel p) {
        FragmentTransaction fragmentTransaction = getChildFragmentManager()
                .beginTransaction();
        fragmentTransaction.remove(p);
        fragmentTransaction.commit();

        panelCount--;
    }

    public void removePanel(SplitPanel p) {
        FragmentTransaction fragmentTransaction = getFragmentManager()
                .beginTransaction();
        fragmentTransaction.remove(p);
        fragmentTransaction.commit();

        panelCount--;
    }

    // change the views size, changing the weight

    public int getHeight() {
        LinearLayout main = (LinearLayout) view.findViewById(R.id.MainLayout);
        return main.getMeasuredHeight();
    }

    public int getWidth() {
        LinearLayout main = (LinearLayout) view.findViewById(R.id.MainLayout);
        return main.getWidth();
    }

    // Save/Load State
    protected void saveState(Editor editor) {
        navigator.saveState(editor);
    }

    protected void loadState(SharedPreferences preferences) {
        if (!navigator.loadState(preferences))
            errorMessage(getString(R.string.error_cannotLoadState));
    }

    public void errorMessage(String message) {
        Context context = getActivity();
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        toast.show();
    }

}
