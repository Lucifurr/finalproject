package com.on.code.object;

public class MenuObj {
	private String title;
	private boolean isSelect;
	private int image;

	public MenuObj(String title,int image, boolean isSelect) {
		this.title = title;
		this.isSelect = isSelect;
		this.image=image;
	}
	
	

	public int getImage() {
		return image;
	}



	public void setImage(int image) {
		this.image = image;
	}



	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public boolean isSelect() {
		return isSelect;
	}

	public void setSelect(boolean isSelect) {
		this.isSelect = isSelect;
	}
}