package com.on.code.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.on.code.BaseActivity;
import com.on.code.R;
import com.on.code.adapter.BookListViewAdapter;
import com.on.code.config.Constant;
import com.on.code.config.GlobalValue;
import com.on.code.database.DatabaseUtility;
import com.on.code.modelmanager.ParserUtility;
import com.on.code.network.ConnectServer;
import com.on.code.object.Movie;
import com.on.code.retrofitobj.BaseMovieRespond;
import com.on.code.util.MySharedPreferences;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("NewApi")
public class SearchActivity extends BaseActivity implements OnClickListener {

    private TextView lblTitle, lblError;
    private ArrayList<Movie> arrMovies;
    private EditText txtSearch;
    private Button btnDelete;
    private ImageView btnTopMenu, btnSearch;
    private Activity self;
    private ListView lsvSearch;
    private BookListViewAdapter adapter;

    private String searchKey = "";
    private int totalPage = 1;
    private int currentPage = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        self = this;
        updateScreenOn();
        initUI();
        initControl();

    }

    public void updateScreenOn() {
        if (GlobalValue.preferences.isScreenOn()) {
            getWindow()
                    .addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        } else {
            getWindow()
                    .clearFlags(
                            WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initUI() {
        lblError = (TextView) findViewById(R.id.lblError);
        btnTopMenu = (ImageView) findViewById(R.id.btnTopLeft);
        lblTitle = (TextView) findViewById(R.id.lblTopTitle);
        txtSearch = (EditText) findViewById(R.id.txtSearch);
        btnSearch = (ImageView) findViewById(R.id.btnSearhNormal);
        btnDelete = (Button) findViewById(R.id.btnDelete);
        lsvSearch = (ListView) findViewById(R.id.lsvSearch);

    }

    private void initControl() {
        lblError.setVisibility(View.VISIBLE);
        lblTitle.setText(getString(R.string.menuSearch));
        initListView();
        btnTopMenu.setOnClickListener(this);
        btnDelete.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                txtSearch.setText("");
            }
        });

        txtSearch.setOnKeyListener(new OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    onSearch();
                }
                return false;
            }
        });

        btnSearch.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                onSearch();
            }
        });
    }


    private void initListView() {
        arrMovies = new ArrayList<>();
        adapter = new BookListViewAdapter(self, arrMovies, new BookListViewAdapter.IOnClickItemListener() {
            @Override
            public void onItemClicked(int position) {
                if (!DatabaseUtility.checkExistsBook(SearchActivity.this, arrMovies
                        .get(position).getId())) {
                    DatabaseUtility.insertHistory(SearchActivity.this,
                            arrMovies.get(position));
                    MySharedPreferences share = new MySharedPreferences(SearchActivity.this);
                    share.putStringValue(Constant.DESC + arrMovies.get(position).getId(), arrMovies.get(position).getDesc());
                    share.putStringValue(Constant.TAGMOVIE + arrMovies.get(position).getId(), GlobalValue.genresIdsToString(arrMovies.get(position).getGenreIds()));
                }

                GlobalValue.movie = arrMovies.get(position);
                GlobalValue.imagebook = arrMovies.get(position).getImage();
                Bundle b = new Bundle();
                b.putParcelable(DetailActivity.KEY_OBJECT, arrMovies.get(position));
                startActivity(DetailActivity.class, b);
            }
        });
        lsvSearch.setAdapter(adapter);
    }

    private void onSearch() {
        searchKey = txtSearch.getText().toString();

        if (!searchKey.isEmpty()) {
            arrMovies.clear();
            adapter.notifyDataSetChanged();
            currentPage = 1;
            totalPage = 1;
            getData(searchKey);

        } else {
            arrMovies.clear();
            adapter.notifyDataSetChanged();
            lblError.setVisibility(View.VISIBLE);
            Toast.makeText(self, "Please input search key !",
                    Toast.LENGTH_SHORT).show();
        }
    }
    private void getData(String query) {
        if (currentPage <= totalPage) {
            showDialog();
            ConnectServer.getResponseAPI().getSearchMovie(Constant.API_KEY,query,currentPage+"").enqueue(new Callback<BaseMovieRespond>() {
                @Override
                public void onResponse(Call<BaseMovieRespond> call, Response<BaseMovieRespond> response) {
                    String json = new Gson().toJson(response.body());
                    ArrayList<Movie> arr = ParserUtility
                            .parseListNewBooks(json);

                    if (arr.size() > 0) {

                        arrMovies.addAll(arr);
                        adapter.notifyDataSetChanged();
                        lblError.setVisibility(View.GONE);

                        currentPage++;
                        totalPage = ParserUtility.getTotalBookPage(json);
                    }
                    closeDialog();
                }

                @Override
                public void onFailure(Call<BaseMovieRespond> call, Throwable t) {
                    closeDialog();
                    Toast.makeText(self, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }

    }

    @Override
    public void onClick(View v) {
        if (v == btnTopMenu) {
            onBackPressed();
        }
    }

}
