package com.on.code.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.internal.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.on.code.BaseFragment;
import com.on.code.R;
import com.on.code.adapter.BookMarkAdapter;
import com.on.code.database.DatabaseUtility;
import com.on.code.object.BookMark;

import java.util.ArrayList;

@SuppressLint("NewApi")
public class BookMarkFragment extends BaseFragment implements OnClickListener {
    private ListView lsvBookMark;
    private BookMarkAdapter adapter;
    private Activity self;
    private ArrayList<BookMark> arrBooks;
    private TextView lblNoBookmarks;
    private Button btnClear;



    public static BookMarkFragment newInstance() {

        Bundle args = new Bundle();

        BookMarkFragment fragment = new BookMarkFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bookmark, container,
                false);
        self = getActivity();
        initUI(view);
        initControl();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setData();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Auto-generated method stub
        super.onCreateOptionsMenu(menu, inflater);
//		inflater.inflate(R.menu.main, menu);
    }

    private void initUI(View view) {
        lsvBookMark = (ListView) view.findViewById(R.id.lsvBookMark);
        lblNoBookmarks = (TextView) view.findViewById(R.id.lblNoBookmarks);
        btnClear = (Button) view.findViewById(R.id.btnClear);
        btnClear.setOnClickListener(this);
    }


    private void initControl() {
        setListView();
    }

    public void setListView() {
        arrBooks = new ArrayList<>();
        adapter = new BookMarkAdapter(getActivity(), arrBooks);
        lsvBookMark.setAdapter(adapter);
//        lsvBookMark.setOnItemClickListener(new OnItemClickListener() {
//
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view,
//                                    int position, long id) {
//
//
//            }
//        });
    }


    public void setData() {
        ArrayList<BookMark> arr = DatabaseUtility.getListBookMarkChapters(self);
        arrBooks.clear();
        arrBooks.addAll(arr);
        adapter.notifyDataSetChanged();
        if (arrBooks.size() == 0) {
            lblNoBookmarks.setVisibility(View.VISIBLE);
            lsvBookMark.setVisibility(View.GONE);
            btnClear.setVisibility(View.GONE);
        } else {
            lblNoBookmarks.setVisibility(View.GONE);
            lsvBookMark.setVisibility(View.VISIBLE);
            btnClear.setVisibility(View.VISIBLE);
        }

    }

    public void updateContent() {
        setData();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnClear:
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        new ContextThemeWrapper(getActivity(), android.R.style.Theme_Holo_Light_Dialog));
                builder.setTitle("Clear BookMark");
                builder.setMessage("Are you sure want to clear?");
                builder.setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();
                            }
                        });
                builder.setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                DatabaseUtility.clearBookMark(getActivity());
                                setData();
                                Toast.makeText(getActivity(), "Successfully!",
                                        Toast.LENGTH_SHORT).show();
                                dialog.dismiss();

                            }
                        });
                builder.create().show();
                break;
        }
    }

    private void startActivity(Class<?> cls, Bundle bundle) {
        Intent intent = new Intent(getActivity(), cls);
        intent.putExtras(bundle);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_in_left,
                R.anim.slide_out_left);
    }
}

