package com.on.code.util;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.LinearLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.on.code.R;

public class GoogleUtility {


	/*
         * create the banner admob on activity
         */
	public static void initBannerAdsOnActivity(Activity act,
			int layoutBannerAdsId) {
		AdView adView = new AdView(act);
		adView.setAdSize(AdSize.BANNER);
		final LinearLayout layout = (LinearLayout) act.findViewById(layoutBannerAdsId);
		adView.setAdUnitId(act.getString(R.string.key_google_admob_banner));

		if (layout != null) {
			layout.addView(adView);
			adView.setAdListener(new AdListener() {
				@Override
				public void onAdLoaded() {
					super.onAdLoaded();
					layout.setVisibility(View.VISIBLE);
				}

				@Override
				public void onAdFailedToLoad(int errorCode) {
					super.onAdFailedToLoad(errorCode);
					layout.setVisibility(View.GONE);
				}
			});

			AdRequest adRequest = new AdRequest.Builder().build();
			adView.loadAd(adRequest);

		}
	}

	/*
	 * create the banner admob on fragment
	 */

	public static void initBannerAdsOnFragment(Fragment act, View view,
			final int layoutBannerAdsId) {
		AdView adView = new AdView(act.getActivity());
		adView.setAdSize(AdSize.BANNER);
		final LinearLayout layout = (LinearLayout) view
				.findViewById(layoutBannerAdsId);
		adView.setAdUnitId(act.getString(R.string.key_google_admob_banner));


		if (layout != null) {
			layout.addView(adView);
			adView.setAdListener(new AdListener() {
				@Override
				public void onAdLoaded() {
					super.onAdLoaded();
					layout.setVisibility(View.VISIBLE);
				}

				@Override
				public void onAdFailedToLoad(int errorCode) {
					super.onAdFailedToLoad(errorCode);
					layout.setVisibility(View.GONE);
				}
			});

			AdRequest adRequest = new AdRequest.Builder().build();
			adView.loadAd(adRequest);

		}
	}

	public static void initInterstitialAdMobOnActivity(Activity act) {
		final InterstitialAd interstitialAd = new InterstitialAd(act);
		interstitialAd.setAdUnitId(act.getString(R.string.key_google_admob_interstitial));
		interstitialAd.setAdListener(new AdListener() {
			@Override
			public void onAdLoaded() {
				// TODO Auto-generated method stub
				interstitialAd.show();
			}
		});
		AdRequest adRequest = new AdRequest.Builder().build();
		interstitialAd.loadAd(adRequest);
	}


}
