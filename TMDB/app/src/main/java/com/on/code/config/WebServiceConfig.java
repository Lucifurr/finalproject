package com.on.code.config;

import android.content.Context;

import com.on.code.R;

public final class WebServiceConfig {
    

    public static String getBooks(Context context) {
        return context.getString(R.string.server_backend_link)  + "movie";
    }

    public static String getMoviesLink(Context context) {
        return context.getString(R.string.server_backend_link) + "movie/now_playing";
    }

    public static String getHot(Context context){
        return context.getString(R.string.server_backend_link) +"movie/popular";
    }

    public static String getRecommended(Context context) {
        return context.getString(R.string.server_backend_link) + "movie/top_rated";
    }

    public static String getMoviesDetail(Context context, String id) {
        return context.getString(R.string.server_backend_link) + "movie/"+ id;
    }

    public static String getGenre(Context context){
        return context.getString(R.string.server_backend_link) + "genre/movie/list";
    }

    public static String getMovieInGenre(Context context, String genreId){
        return context.getString(R.string.server_backend_link) + "genre/"+genreId+"/movies";

    }

    public static String getSearchMovie(Context context){
        return context.getString(R.string.server_backend_link) + "search/movie";

    }

    public static String getImageLink(String size, String path ){
        return "https://image.tmdb.org/t/p/" + size+ "/" + path;

    }
    public static String getMovieLink(Context context, String movieId){
        return context.getString(R.string.server_backend_link) + "movie/"+ movieId +"/videos";

    }

    public static String getCredid(Context context,String movieID){
        return context.getString(R.string.server_backend_link) + "movie/"+ movieID +"/credits";
    }






    public static String getTopViewed(Context context) {
        return context.getString(R.string.server_backend_link) + "top_view";
    }
    public static String getLastChaptersBook(Context context) {
        return context.getString(R.string.server_backend_link) + "last_chapter_by_book";
    }
    public static String getCountViewBook(Context context) {
        return context.getString(R.string.server_backend_link) + "view_book";
    }
    public static String getBookById(Context context) {
        return context.getString(R.string.server_backend_link) + "movie";
    }
    public static String getCategory(Context context) {
        return context.getString(R.string.server_backend_link) + "category";
    }
    public static String getBookByCategory(Context context) {
        return context.getString(R.string.server_backend_link) + "book_by_category";
    }
    public static String addComment(Context context) {
        return context.getString(R.string.server_backend_link) + "addcomment";
    }
    public static String getCommentByChapter(Context context) {
        return context.getString(R.string.server_backend_link) + "comment";
    }
    public static String getListChapterByBookId(Context context) {
        return context.getString(R.string.server_backend_link) + "search_chapter";
    }
    public static String searchBooksByKey(Context context) {
        return context.getString(R.string.server_backend_link) + "searchbook";
    }
    public static String getChaptersBySearchKey(Context context) {
        return context.getString(R.string.server_backend_link) + "searchchapter";
    }
    public static String getListQuotesByChapterId(Context context) {
        return context.getString(R.string.server_backend_link) + "search_quote";
    }
    public static String sendFeedback(Context context) {
        return context.getString(R.string.server_backend_link) + "feedback";
    }
}
