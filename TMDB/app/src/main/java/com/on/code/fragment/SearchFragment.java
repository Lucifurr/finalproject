package com.on.code.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.on.code.BaseFragment;
import com.on.code.R;
import com.on.code.activity.DetailActivity;
import com.on.code.adapter.BookListViewAdapter;
import com.on.code.config.Constant;
import com.on.code.config.GlobalValue;
import com.on.code.database.DatabaseUtility;
import com.on.code.modelmanager.ParserUtility;
import com.on.code.network.ConnectServer;
import com.on.code.object.Movie;
import com.on.code.retrofitobj.BaseMovieRespond;
import com.on.code.util.MySharedPreferences;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("NewApi")
public class SearchFragment extends BaseFragment implements OnClickListener {

    private TextView lblError;
    private EditText txtSearch;
    private Button btnDelete;
    private ImageView btnSearch;
    private Activity self;
    private ListView lsvSearch;
    private ArrayList<Movie> arrSearch;
    private BookListViewAdapter adapter;

    private String searchKey = "";
    private int totalPage = 1;
    private int currentPage = 1;


    public static SearchFragment newInstance() {

        Bundle args = new Bundle();

        SearchFragment fragment = new SearchFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater
                .inflate(R.layout.fragment_search, container, false);
        self = getActivity();
        currentPage = 1;

        initUI(view);
        initControl();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initUI(View view) {
        lblError = (TextView) view.findViewById(R.id.lblError);
        txtSearch = (EditText) view.findViewById(R.id.txtSearch);
        btnSearch = (ImageView) view.findViewById(R.id.btnSearhNormal);
        btnDelete = (Button) view.findViewById(R.id.btnDelete);
        lsvSearch = (ListView) view.findViewById(R.id.lsvSearch);
        self.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

    }

    private void initControl() {
        lblError.setVisibility(View.VISIBLE);
        initListView();
        btnDelete.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                txtSearch.setText("");
            }
        });
        txtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    onSearch();
                    return true;
                }
                return false;
            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                onSearch();
            }
        });
    }

    private void initListView() {
        arrSearch = new ArrayList<>();
        adapter = new BookListViewAdapter(self, arrSearch, new BookListViewAdapter.IOnClickItemListener() {
            @Override
            public void onItemClicked(int position) {
                if (DatabaseUtility.checkExistsBook(getActivity(), arrSearch.get(position).getId())) {
                    Log.e("", "DATA " + "data da co du lieu");
                } else {
                    DatabaseUtility.insertHistory(getActivity(), arrSearch.get(position));
                    MySharedPreferences share = new MySharedPreferences(getActivity().getBaseContext());
                    share.putStringValue(Constant.DESC + arrSearch.get(position).getId(), arrSearch.get(position).getDesc());
                    share.putStringValue(Constant.TAGMOVIE + arrSearch.get(position).getId(), GlobalValue.genresIdsToString(arrSearch.get(position).getGenreIds()));
                }
                Bundle b = new Bundle();
                b.putParcelable(DetailActivity.KEY_OBJECT, arrSearch.get(position));

                startActivity(DetailActivity.class, b);
            }
        });
        lsvSearch.setAdapter(adapter);
    }

    private void onSearch() {

        searchKey = txtSearch.getText().toString();

        if (!searchKey.isEmpty()) {
            arrSearch.clear();
            adapter.notifyDataSetChanged();
            currentPage = 1;
            totalPage = 1;
            getData(searchKey);

        } else {
            arrSearch.clear();
            adapter.notifyDataSetChanged();
            lblError.setVisibility(View.VISIBLE);
            Toast.makeText(self, "Please input search key !",
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void getData(String query) {
        if (currentPage <= totalPage) {
            showDialog();
            ConnectServer.getResponseAPI().getSearchMovie(Constant.API_KEY, query, currentPage + "").enqueue(new Callback<BaseMovieRespond>() {
                @Override
                public void onResponse(Call<BaseMovieRespond> call, Response<BaseMovieRespond> response) {
                    String json = new Gson().toJson(response.body());
                    ArrayList<Movie> arr = ParserUtility
                            .parseListNewBooks(json);

                    if (arr.size() > 0) {

                        arrSearch.addAll(arr);
                        adapter.notifyDataSetChanged();
                        lblError.setVisibility(View.GONE);

                        currentPage++;
                        totalPage = ParserUtility.getTotalBookPage(json);
                    }
                    closeDialog();
                }

                @Override
                public void onFailure(Call<BaseMovieRespond> call, Throwable t) {
                    closeDialog();
                    Toast.makeText(self, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }

    }

    @Override
    public void onClick(View v) {

    }

    private void startActivity(Class<?> cls, Bundle bundle) {
        Intent intent = new Intent(self, cls);
        intent.putExtras(bundle);
        startActivity(intent);
        self.overridePendingTransition(R.anim.slide_in_left,
                R.anim.slide_out_left);
    }

}
