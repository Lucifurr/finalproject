package com.on.code.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.on.code.R;
import com.on.code.config.GlobalValue;
import com.on.code.object.Quote;

@SuppressLint("NewApi")
public class QuoteAdapter extends BaseAdapter{
	private String TAG = "QuoteAdapter.java";
	private List<Quote> listBaptismalInfo;
	private LayoutInflater inflater = null;
	private Activity act;
	private String searchKey = "";
	private AQuery aq;
	private boolean memCache = false;
	private boolean fileCache = true;
	private Dialog dialog;

	private Handler mHandler = new Handler();

	public QuoteAdapter(Activity activity, List<Quote> listCategoryInfo,
			String searchKey) {
		this.listBaptismalInfo = listCategoryInfo;
		this.act = activity;
		this.searchKey = searchKey;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public int getCount() {
		return listBaptismalInfo.size();
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}


	@SuppressWarnings("null")
	@Override
	public View getView(int position, View convertView, final ViewGroup parent) {
		final ViewHolder holder;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.row_item_quote, null);
			holder = new ViewHolder();
			holder.lblContent = (TextView) convertView
					.findViewById(R.id.lblContent);
			holder.image = (ImageView) convertView
					.findViewById(R.id.imgContent);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		// set data at here

		// setChanel(holder);
		// initControl(holder);
		// playAudio("");
		// setTvSize(holder);
		Quote item = listBaptismalInfo.get(position);
		if (item != null) {
			if (!searchKey.isEmpty()) {
				String content = item.getContent().replace(searchKey,
						"<font color=\"blue\">" + searchKey + "</font>");
				searchKey = searchKey.replace(searchKey.charAt(0),
						Character.toUpperCase(searchKey.charAt(0)));
				content = content.replace(searchKey, "<font color=\"blue\">"
						+ searchKey + "</font>");
				searchKey = searchKey.replace(searchKey.charAt(0),
						Character.toLowerCase(searchKey.charAt(0)));
				content = content.replace(searchKey, "<font color=\"blue\">"
						+ searchKey + "</font>");

				holder.lblContent.setText(Html.fromHtml(content).toString());
				holder.lblContent.setMovementMethod(LinkMovementMethod
						.getInstance());
			} else {
				holder.lblContent.setText(Html.fromHtml(item.getContent()));
				holder.lblContent.setMovementMethod(LinkMovementMethod
						.getInstance());

			}
			if (item.getType() == 1) {
				Log.e("QuoteAdapter", "image = " + item.getImage());
				if (!item.getImage().isEmpty()) {
					aq = new AQuery(act);
					aq.id(holder.image).image(item.getImage(), memCache,
							fileCache);
					holder.image.setVisibility(View.VISIBLE);
				} else {
					holder.image.setVisibility(View.GONE);
				}
			}else
			{
				holder.image.setVisibility(View.GONE);
			}
		}
		if (GlobalValue.preferences != null) {
			GlobalValue.setTypeFace(holder.lblContent);
		}
		return convertView;
	}

	private class ViewHolder {
		private TextView lblContent;
		private ImageView image;

	}


}
