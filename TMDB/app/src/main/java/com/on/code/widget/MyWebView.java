package com.on.code.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import nl.siegmann.epublib.domain.Book;


public class MyWebView extends WebView {

    private Book book;
    public MyWebView(Context context) {
        super(context);
    }

    @Override
    public void setWebViewClient(WebViewClient client) {
        super.setWebViewClient(new MyWebClient());
    }

    private class MyWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {

            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);



            String varMySheet = "var mySheet = document.styleSheets[0];";

            String addCSSRule = "function addCSSRule(selector, newRule) {"
                    + "ruleIndex = mySheet.cssRules.length;"
                    + "mySheet.insertRule(selector + '{' + newRule + ';}', ruleIndex);"

                    + "}";

            String insertRule1 = "addCSSRule('html', 'padding: 0px; height: "
                    + (view.getMeasuredHeight() / getContext().getResources().getDisplayMetrics().density)
                    + "px; -webkit-column-gap: 0px; -webkit-column-width: "
                    + view.getMeasuredWidth() + "px;')";


            view.loadUrl("javascript:" + varMySheet);
            view.loadUrl("javascript:" + addCSSRule);
            view.loadUrl("javascript:" + insertRule1);
        }
    }
}
