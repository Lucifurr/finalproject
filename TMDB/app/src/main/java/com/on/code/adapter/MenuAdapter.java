package com.on.code.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.on.code.R;
import com.on.code.object.MenuObj;

public class MenuAdapter extends BaseAdapter {
	private List<MenuObj> listMenus;
	private LayoutInflater inflater;
	private Activity act;

	public MenuAdapter(Activity activity, List<MenuObj> listMenus) {
		this.listMenus = listMenus;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.act = activity;
	}

	@Override
	public int getCount() {
		return listMenus.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressLint("ResourceAsColor")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.item_menu, null);
			holder.menuName = (TextView) convertView
					.findViewById(R.id.lblNameMenu);
			holder.iconMenu = (ImageView) convertView
					.findViewById(R.id.imgMenu);
			holder.layoutMenuRow = (LinearLayout) convertView
					.findViewById(R.id.layoutMenuRow);
            holder.divider = convertView.findViewById(R.id.vw_divider);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		MenuObj o = listMenus.get(position);
		if (o != null) {
			holder.menuName.setText(o.getTitle());
			holder.iconMenu.setImageResource(o.getImage());
			if (o.isSelect()) {
				holder.layoutMenuRow.setBackgroundResource(R.color.item_sliMenu);
			} else {
				holder.layoutMenuRow.setBackgroundResource(R.color.bg_slidingMenu);
			}

            // Show divider
            if (position == 4 || position == 6) {
                holder.divider.setVisibility(View.VISIBLE);
            } else {
                holder.divider.setVisibility(View.GONE);
            }
		}
		return convertView;
	}

	private class ViewHolder {
		private LinearLayout layoutMenuRow;
		private ImageView iconMenu;
		private TextView menuName;
        private View divider;
	}
}
