package com.on.code.retrofitobj;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VideoMovieRespond {

    @SerializedName("results")
    private List<Results> results;
    @SerializedName("id")
    private int id;

    public List<Results> getResults() {
        return results;
    }

    public void setResults(List<Results> results) {
        this.results = results;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public static class Results {
        @SerializedName("type")
        private String type;
        @SerializedName("size")
        private int size;
        @SerializedName("site")
        private String site;
        @SerializedName("name")
        private String name;
        @SerializedName("key")
        private String key;
        @SerializedName("iso_3166_1")
        private String iso_3166_1;
        @SerializedName("iso_639_1")
        private String iso_639_1;
        @SerializedName("id")
        private String id;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public String getSite() {
            return site;
        }

        public void setSite(String site) {
            this.site = site;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getIso_3166_1() {
            return iso_3166_1;
        }

        public void setIso_3166_1(String iso_3166_1) {
            this.iso_3166_1 = iso_3166_1;
        }

        public String getIso_639_1() {
            return iso_639_1;
        }

        public void setIso_639_1(String iso_639_1) {
            this.iso_639_1 = iso_639_1;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}
