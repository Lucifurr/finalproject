package com.on.code.adapter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.on.code.R;
import com.on.code.config.GlobalValue;
import com.on.code.object.CommentObj;
import com.on.code.util.DateTimeUtility;

public class CommentAdapter extends BaseAdapter {
	private Activity mainActivity;
	private List<CommentObj> listComment;
	private LayoutInflater layoutInflater;

	public CommentAdapter(Activity activity,
			ArrayList<CommentObj> listverseVerses) {
		this.mainActivity = activity;
		this.listComment = listverseVerses;
		this.layoutInflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public int getCount() {
		return listComment.size();
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		// TODO Auto-generated method stub
		final Hoder holder;
		if (convertView == null) {
			holder = new Hoder();
			convertView = layoutInflater.inflate(R.layout.item_layout_comment,
					null);
			holder.lblName = (TextView) convertView.findViewById(R.id.lblName);
			holder.lblComment = (TextView) convertView
					.findViewById(R.id.lblComment);
			holder.lblDate = (TextView) convertView
					.findViewById(R.id.lblDatetime);

			convertView.setTag(holder);

		} else {
			holder = (Hoder) convertView.getTag();
		}

		CommentObj comment = listComment.get(position);
		if (comment != null) {
			if (GlobalValue.preferences != null) {
				GlobalValue.setTypeFont(holder.lblComment,
						holder.lblDate);
			}
			holder.lblName.setText(comment.getName());
			holder.lblComment.setText(comment.getContent());
			// Set time
			String time = "";
			Date curDate = Calendar.getInstance().getTime();
			Date specDate = DateTimeUtility.getDateValueFromString(
					"yyyy-MM-dd HH:mm:ss", comment.getDatetime());

			long minuteDiff = DateTimeUtility.getDateDiff(curDate, specDate,
					TimeUnit.MINUTES);
			long hourDiff = DateTimeUtility.getDateDiff(curDate, specDate,
					TimeUnit.HOURS);
			long dateDiff = DateTimeUtility.getDateDiff(curDate, specDate,
					TimeUnit.DAYS);

			if (minuteDiff <= 1) {
				time = parent.getContext().getResources()
						.getString(R.string.just_now);
			} else if (minuteDiff > 1 && minuteDiff <= 59) {
				time = minuteDiff
						+ " "
						+ parent.getContext().getResources()
						.getString(R.string.minutes_ago);
			} else if (minuteDiff > 59) {
				// Reset minute var
				minuteDiff = 0;

				if (hourDiff == 1) {
					time = hourDiff
							+ " "
							+ parent.getContext().getResources()
							.getString(R.string.hour_ago);
				} else if (hourDiff < 24) {
					time = hourDiff
							+ " "
							+ parent.getContext().getResources()
							.getString(R.string.hours_ago);
				} else {
					// Reset hour var
					hourDiff = 0;

					if (dateDiff == 0) {
						time = parent.getContext().getResources()
								.getString(R.string.today);
					} else if (dateDiff == 1) {
						time = parent.getContext().getResources()
								.getString(R.string.yesterday);
					} else if (dateDiff == 2) {
						time = parent.getContext().getResources()
								.getString(R.string.two_days_ago);
					} else if (dateDiff == 3) {
						time = parent.getContext().getResources()
								.getString(R.string.three_days_ago);
					} else {
						// Reset date var
						dateDiff = 0;

						time = DateTimeUtility.convertStringToDate(
								comment.getDatetime(), "yyyy-MM-dd HH:mm:ss",
								"EEE, dd MMM yyyy");
					}
				}
			}

			holder.lblDate.setText(time);
		}

		return convertView;
	}

	static class Hoder {
		TextView lblName, lblComment, lblDate;
	}
}
