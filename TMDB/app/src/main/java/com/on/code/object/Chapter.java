package com.on.code.object;

import java.io.File;
import java.util.ArrayList;

import com.on.code.config.GlobalValue;
import com.on.code.database.DatabaseUtility;
import com.on.code.util.StringUtility;

import android.app.Activity;
import android.util.Log;

public class Chapter {
    public static final int TYPE_PDF = 1;
    public static final int TYPE_EPUB = 2;
    public static final int TYPE_VIDEO = 3;
    public static final int TYPE_NORMAL = 0;
    private String id;
    private String bookId;
    private String title;
    private String currentBookMarkIndex;
    private String image;
    private String titleBook;
    private int type;
    private String attachedFile;
//    private String directUrl;

    ArrayList<Chapter> arr = new ArrayList<Chapter>();

//
//    public String getDirectUrl() {
//        return directUrl;
//    }
//
//    public void setDirectUrl(String directUrl) {
//        this.directUrl = directUrl;
//    }

    public int getType() {
        return type;
    }

    public String getAttachedFile() {
        return attachedFile;
    }

    public void setAttachedFile(String attachedFile) {
        this.attachedFile = attachedFile;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTitleBook() {
        return titleBook;
    }

    public void setTitleBook(String titleBook) {
        this.titleBook = titleBook;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public String getCurrentBookMarkIndex() {
        return currentBookMarkIndex;
    }

    public void setCurrentBookMarkIndex(String currentBookMarkIndex) {
        this.currentBookMarkIndex = currentBookMarkIndex;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isBookMark(Activity act) {
        return DatabaseUtility.checkChapterisBookMark(act, bookId, id);
    }

    public int getPositionQuote(Activity act) {
        if (isBookMark(act)) {
            BookMark bookmark = DatabaseUtility
                    .getBookmarkById(act, bookId, id);
            return bookmark.getQuoteIndex();
        }
        return 0;
    }

    public int getChapterIndex(Activity act, ArrayList<Chapter> arrChapter) {

        for (int i = 0; i < arrChapter.size(); i++) {
            //
            if (id.equals(arrChapter.get(i).getId())) {
                Log.e("chapter.111", id + " ==== " + arrChapter.get(i).getId());
                Log.e("chapter.111", "position: " + i);
                return i;
            }
        }
        return 0;
    }

    public boolean isCacheAttachedFile() {
        String fileUrl = GlobalValue.CACHE_FOLDER + StringUtility.getFullFileNameFromUrl(getAttachedFile());
        File f = new File(fileUrl);
        return f.exists();
    }

    public String getCacheAttachedFileUrl() {
        return GlobalValue.CACHE_FOLDER + StringUtility.getFullFileNameFromUrl(getAttachedFile());
    }

}
