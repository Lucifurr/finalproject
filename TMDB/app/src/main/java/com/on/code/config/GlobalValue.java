package com.on.code.config;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Environment;
import android.widget.TextView;

import com.on.code.PacketUtility;
import com.on.code.object.Movie;
import com.on.code.object.CatagoryObj;
import com.on.code.object.Chapter;
import com.on.code.object.MovieDetail;
import com.on.code.util.MySharedPreferences;

/**
 * GlobalValue class contains global static values
 * 
 * @author Lemon
 */
public final class GlobalValue {
	public static String CACHE_FOLDER = Environment.getExternalStorageDirectory() + "/" + PacketUtility.getPacketName() + "/cache/";
	public static Chapter currentChapter;
	public static String ChapterId;
	public static String imagebook;
	public static int check = 0;
	public static MySharedPreferences preferences;
	private static Typeface typeface;
	public static ArrayList<Chapter> arrChapter = new ArrayList<Chapter>();
	public static Movie movie;
	public static int screenWidth;
	public static int screenHeight;
	private static ArrayList<CatagoryObj> arrCategory;

	public static void constructor(Context context) {
		if (preferences == null) {
			preferences = new MySharedPreferences(context);
		}

		if (GlobalValue.preferences.getFont().length() == 0) {
			GlobalValue.preferences.putFont("fonts/Roboto-Light.ttf");
		}

		if (GlobalValue.preferences.getFontSize() == 0) {
			GlobalValue.preferences.putFontSize(20);
		}

		createTypeFace(context);
	}

	public static void createTypeFace(Context context) {
		typeface = Typeface.createFromAsset(context.getAssets(),
				preferences.getFont());
	}

	public static void setTypeFace(TextView... lbl) {
		for (TextView textView : lbl) {
			textView.setTextSize(preferences.getFontSize());
			textView.setTypeface(typeface);
		}
	}

	public static void setTypeFont(TextView... lbl) {
		for (TextView textView : lbl) {
			textView.setTypeface(typeface);
		}
	}

	public static void setTypeFaceBold(TextView... lbl) {
		float size = preferences.getFontSize();
		for (TextView textView : lbl) {
			textView.setTextSize(size);
			textView.setTypeface(typeface, Typeface.BOLD);
		}
	}

	public static void setTypeFace(Context context, String font, float size,
			TextView... lbl) {
		Typeface tf = Typeface.createFromAsset(context.getAssets(), font);
		for (TextView textView : lbl) {
			textView.setTextSize(size);
			textView.setTypeface(tf);
		}
	}

	public static void setCategory(ArrayList<CatagoryObj> list){
		arrCategory = list;
	}

	public static ArrayList<CatagoryObj> getCategoires(){
		if (arrCategory == null)
			arrCategory= new ArrayList<>();
		return  arrCategory;
	}

	public static String genresIdsToString(List<String> ids){
		String str = "";
		for (String id: ids) {
			for (CatagoryObj item: getCategoires()) {
				if (id.equals(item.getId()+"")){
					str += item.getTitle() +", ";
					break;
				}
			}
		}

		return str;
	}

	public static String genresToString(List<MovieDetail.Genres> genres){
		String s = "";
		for (int i = 0; i <genres.size() ; i++) {
			if(i==0){
				s += genres.get(i).getName();
			} else {
				s += ", "+ genres.get(i).getName();
			}
		}
		return s;
	}

}
