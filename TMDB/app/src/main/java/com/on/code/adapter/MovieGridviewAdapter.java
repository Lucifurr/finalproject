package com.on.code.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.on.code.R;
import com.on.code.object.Movie;

public class MovieGridviewAdapter extends BaseAdapter {
    private Activity mainActivity;
    private List<Movie> listVerses;
    private LayoutInflater layoutInflater;
    private AQuery aq;
    private boolean memCache = false;
    private boolean fileCache = true;

    public MovieGridviewAdapter(Activity activity, List<Movie> listverseVerses) {
        this.mainActivity = activity;
        this.listVerses = listverseVerses;
        this.layoutInflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return listVerses.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // TODO Auto-generated method stub
        final Hoder holder;
        if (convertView == null) {
            holder = new Hoder();
            convertView = layoutInflater.inflate(R.layout.item_book, null);
            holder.lblTitle = (TextView) convertView
                    .findViewById(R.id.lblTitle);
            holder.lblTitle.setSelected(true);
            holder.lblChaplast = (TextView) convertView
                    .findViewById(R.id.lblChapterlast);
            holder.imgBook = (ImageView) convertView
                    .findViewById(R.id.imgBookIcon);

            convertView.setTag(holder);

        } else {
            holder = (Hoder) convertView.getTag();
        }

        Movie movie = listVerses.get(position);
        if (movie != null) {
            aq = new AQuery(mainActivity);
            holder.lblTitle.setText(movie.getTitle().toString());
            holder.lblChaplast.setText(movie.getReleaseDate().substring(0,4));
            //holder.lblChaplast.setText(movie.getTitleChapter());
//            if (GlobalValue.preferences != null) {
//                GlobalValue.setTypeFont(holder.lblTitle, holder.lblChaplast);
//            }
            aq.id(holder.imgBook).image(movie.getImage(), memCache, fileCache);

        }

        return convertView;
    }

    static class Hoder {
        TextView lblTitle, lblChaplast;
        ImageView imgBook;
    }
}
