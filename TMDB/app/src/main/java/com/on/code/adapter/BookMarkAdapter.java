package com.on.code.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.on.code.R;
import com.on.code.activity.DetailActivity;
import com.on.code.activity.MainSherkLockActivity;
import com.on.code.config.Constant;
import com.on.code.config.GlobalValue;
import com.on.code.object.Movie;
import com.on.code.object.BookMark;
import com.on.code.util.MySharedPreferences;

public class BookMarkAdapter extends BaseAdapter {
    private Context mainActivity;
    private List<BookMark> listVerses;
    private LayoutInflater layoutInflater;
    private AQuery aq;
    private boolean memCache = false;
    private boolean fileCache = true;


    public BookMarkAdapter(Context activity,
                           ArrayList<BookMark> listverseVerses) {
        this.mainActivity = activity;
        this.listVerses = listverseVerses;
        this.layoutInflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return listVerses.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        // TODO Auto-generated method stub
        final Hoder holder;
        if (convertView == null) {
            holder = new Hoder();
            convertView = layoutInflater.inflate(R.layout.item_list_main, null);
            holder.lblChapterTitle = (TextView) convertView
                    .findViewById(R.id.lblChapterTitle);
            holder.lblChapterTitle.setSelected(true);
            holder.lblBookTitle = (TextView) convertView
                    .findViewById(R.id.lblBookTitle);
            holder.imgBook = (ImageView) convertView
                    .findViewById(R.id.imgBookIcon);
            holder.lblDesc = (TextView) convertView.findViewById(R.id.lblDesc);
            holder.lblYear = (TextView) convertView
                    .findViewById(R.id.lblYear);
            holder.rbVote = (RatingBar) convertView.findViewById(R.id.rbVoting);
            convertView.setTag(holder);

        } else {
            holder = (Hoder) convertView.getTag();
        }

        final BookMark bookmark = listVerses.get(position);
        MySharedPreferences shares = new MySharedPreferences(mainActivity);
        if (shares.getBooleanValue(bookmark.getBookId())) {
            Log.d("ABCDDDDDDD", "getView: " + bookmark.getId() + shares.getBooleanValue(bookmark.getId()));
            if (bookmark != null) {
                aq = new AQuery(mainActivity);
                if (GlobalValue.preferences != null) {
                    GlobalValue.setTypeFont(holder.lblChapterTitle,
                            holder.lblBookTitle);
                }
                holder.lblChapterTitle.setText(bookmark.getBookTitle());


                String tag = shares.getStringValue(Constant.TAGMOVIE + bookmark.getBookId());
                Log.d("", "getView: " + tag);
//			holder.lblBookTitle.setText(bookmark.getBookId());
                holder.lblBookTitle.setText(tag);

                if (bookmark.getChapterId() != null && bookmark.getChapterId().length()>5)
                    holder.lblYear.setText(bookmark.getChapterId().substring(0, 4));
                else
                    holder.lblYear.setText("");
                holder.lblDesc.setText(bookmark.getChapterTitle());
                holder.rbVote.setRating( Float.valueOf(bookmark.getChapterIndex()) / 2);
                aq.id(holder.imgBook).image(bookmark.getImage(), memCache,
                        fileCache);


                //holder.lblYear.setText(movie.getReleaseDate().substring(0,4));
                //holder.rbVote.setRating((float) movie.getVoteAverage() / 2);

                LayerDrawable stars = (LayerDrawable) holder.rbVote.getProgressDrawable();
                stars.getDrawable(2).setColorFilter(mainActivity.getResources().getColor(R.color.all_color), PorterDuff.Mode.SRC_ATOP);

            }
        }

//        convertView.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View view) {
//                AlertDialog.Builder builder = new AlertDialog.Builder(
//                        new ContextThemeWrapper(mainActivity, android.R.style.Theme_Holo_Light_Dialog));
//                builder.setTitle("Clear History");
//                builder.setMessage("Are you sure want to clear?");
//                builder.setNegativeButton("Cancel",
//                        new DialogInterface.OnClickListener() {
//
//                            @Override
//                            public void onClick(DialogInterface dialog,
//                                                int which) {
//                                dialog.dismiss();
//                            }
//                        });
//                builder.setPositiveButton("Ok",
//                        new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog,
//                                                int which) {
//                                DatabaseUtility.removeBookMark(mainActivity, bookmark.getBookId());
//                                listVerses.clear();
//                                listVerses =  DatabaseUtility.getListBookMarkChapters(mainActivity);
//                                notifyDataSetChanged();
//                                Toast.makeText(mainActivity, "Successfully!",
//                                        Toast.LENGTH_SHORT).show();
//                                dialog.dismiss();
//
//                            }
//                        });
//                builder.create().show();
//                return true;
//            }
//        });
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GlobalValue.movie = new Movie();
                GlobalValue.imagebook = bookmark.getImage();
                Bundle b = new Bundle();
                b.putParcelable(DetailActivity.KEY_OBJECT, bookmark.convertToBook());
                b.putBoolean("NON_NEW",true);
                ((MainSherkLockActivity)mainActivity).startActivity(DetailActivity.class, b);
            }
        });
        return convertView;
    }

    static class Hoder {
        RatingBar rbVote;
        TextView lblChapterTitle, lblBookTitle, lblDesc, lblYear;
        ImageView imgBook;
    }
}
