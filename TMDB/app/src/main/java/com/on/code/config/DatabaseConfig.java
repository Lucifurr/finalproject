package com.on.code.config;

import android.annotation.SuppressLint;

import com.on.code.PacketUtility;

public final class DatabaseConfig {
	private final int DB_VERSION = 3;
	private final String DB_NAME = "BookAppDb.sqlite";

	public static String TABLE_BOOK = "tblBook";
	public static String TABLE_CHAPTER = "tblChapter";
	public static String TABLE_BOOK_MARK = "tblBookMark";
	public static String TABLE_QUOTE = "tblQuote";
	public static String TABLE_HISTORY = "tblHistory";

	/**
	 * Get database version
	 * 
	 * @return
	 */
	public int getDatabaseVersion() {
		return DB_VERSION;
	}

	/**
	 * Get database name
	 * 
	 * @return
	 */
	public String getDatabaseName() {
		return DB_NAME;
	}

	/**
	 * Get database path
	 * 
	 * @return
	 */
	@SuppressLint("SdCardPath")
	public String getDatabasePath() {
		return "/data/data/" + PacketUtility.getPacketName() + "/databases/";
	}

	/**
	 * Get database path
	 * 
	 * @return
	 */
	public String getDatabaseFullPath() {
		return getDatabasePath() + DB_NAME;
	}
}
