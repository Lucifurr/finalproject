package com.on.code.activity;

import android.app.FragmentTransaction;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.google.gson.Gson;
import com.on.code.BaseActivity;
import com.on.code.R;
import com.on.code.config.Constant;
import com.on.code.config.GlobalValue;
import com.on.code.database.DatabaseUtility;
import com.on.code.modelmanager.ParserUtility;
import com.on.code.network.ConnectServer;
import com.on.code.object.Movie;
import com.on.code.object.Chapter;
import com.on.code.object.Credits;
import com.on.code.object.MovieDetail;
import com.on.code.object.VideoLink;
import com.on.code.retrofitobj.MovieCreditRespond;
import com.on.code.retrofitobj.MovieDetailRespond;
import com.on.code.retrofitobj.VideoMovieRespond;
import com.on.code.util.MySharedPreferences;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DetailActivity extends BaseActivity implements View.OnClickListener {

    public static final String KEY_OBJECT = "KEY_OBJECT";
    private TextView tvName, tvYear, tvOverview, tvTitle, tvDirector, tvCharactor, tvGenres, tvProducer;
    private RatingBar rb;
    private ImageView btnBack, imgVideo, imgPlay;
    private Button btnSave;
    private Movie mMovie;
    private FrameLayout frmPlayer;
    private YouTubePlayerFragment playerFragment;

    private MovieDetail movie;
    private Credits credits;

    private int id = 0;

    private AdView mBannerAd;
    private InterstitialAd mInterstitialAd;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        initView();
        getExtraData();
        initPlayer();
        getLinkPlay();
        showBannerAd();
    }

    private void initView() {
        tvName = (TextView) findViewById(R.id.lblName);
        tvYear = (TextView) findViewById(R.id.lblYear);
        tvOverview = (TextView) findViewById(R.id.tvOverview);
        tvTitle = (TextView) findViewById(R.id.lblHeaderTitle);
        rb = (RatingBar) findViewById(R.id.rbVoting);
        btnBack = (ImageView) findViewById(R.id.btnTopBack);
        btnSave = (Button) findViewById(R.id.btnBookMark);
        frmPlayer = (FrameLayout) findViewById(R.id.frm_content);
        tvDirector = (TextView) findViewById(R.id.tvDirector);
        tvCharactor = (TextView) findViewById(R.id.tvCharactor);
        tvGenres = (TextView) findViewById(R.id.tvGenres);
        tvProducer = (TextView) findViewById(R.id.tvProducer);
        mBannerAd = (AdView) findViewById(R.id.banner_AdView);

        btnSave.setOnClickListener(this);
        btnBack.setOnClickListener(this);


    }

    private void showBannerAd() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("F3E32BC782A2C6C8EF63426CAD7786D2")
                .build();
        mBannerAd.loadAd(adRequest);
    }

    @Override
    public void onClick(View view) {
        if (view == btnSave) {
            bookMakr();
        } else if (view == btnBack) {
            onBackPressed();
        }
    }

    private void getExtraData() {

        if (getIntent().getExtras().getBoolean("NON_NEW")) {
            mMovie = getIntent().getExtras().getParcelable(KEY_OBJECT);
            if (mMovie != null) {
                id = Integer.valueOf(mMovie.getId());
                getMovieDetail();
                getCredits();
            }
        } else {
            mMovie = getIntent().getExtras().getParcelable(KEY_OBJECT);
            showData();
            getCredits();
        }

    }

    private void showData() {
        MySharedPreferences shares = new MySharedPreferences(this);
        String tag = shares.getStringValue(Constant.TAGMOVIE + mMovie.getId());

        tvName.setText(mMovie.getTitle());
        String year = "";
        if (mMovie.getReleaseDate() != null && mMovie.getReleaseDate().length() > 5) {
            year = mMovie.getReleaseDate().substring(0, 4);
        }
        tvYear.setText(year);
        tvOverview.setText(mMovie.getDesc());
        tvGenres.setText(tag);
        rb.setRating((float) mMovie.getVoteAverage() / 2);
        LayerDrawable stars = (LayerDrawable) rb.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(getResources().getColor(android.R.color.holo_blue_bright), PorterDuff.Mode.SRC_ATOP);

        if (DatabaseUtility.checkExitsBookMarkinBook(this, mMovie.getId())) {
            updateBookMark(true);
        } else {
            updateBookMark(false);
        }
    }


    private void initPlayer() {
        playerFragment = YouTubePlayerFragment.newInstance();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.frm_content, playerFragment);
        transaction.commit();
    }

    private void getLinkPlay() {
        showDialog();
        ConnectServer.getResponseAPI().getMovieLink(mMovie.getId(), Constant.API_KEY).enqueue(new Callback<VideoMovieRespond>() {
            @Override
            public void onResponse(Call<VideoMovieRespond> call, Response<VideoMovieRespond> response) {
                String json = new Gson().toJson(response.body());
                List<VideoLink> arr = ParserUtility.parseListLink(json);
                mMovie.setLinks(arr);
                checkAndPlay();
                closeDialog();
            }

            @Override
            public void onFailure(Call<VideoMovieRespond> call, Throwable t) {
                closeDialog();
                Toast.makeText(DetailActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getCredits() {
        showDialog();
        ConnectServer.getResponseAPI().getCredit(mMovie.getId(), Constant.API_KEY).enqueue(new Callback<MovieCreditRespond>() {
            @Override
            public void onResponse(Call<MovieCreditRespond> call, Response<MovieCreditRespond> response) {
                String json = new Gson().toJson(response.body());
                credits = ParserUtility.getCredistDetail(json);
                List<Credits.Cast> arrcast = new ArrayList<Credits.Cast>();
                List<Credits.Crew> arrCrew = new ArrayList<Credits.Crew>();
                arrcast = credits.getCast();
                arrCrew = credits.getCrew();
                String charractor = "";
                String dirrector = "";
                for (int i = 0; i < arrcast.size(); i++) {
                    if (i == 0) {
                        charractor += arrcast.get(i).getCharacter();
                    } else {
                        charractor += ", " + arrcast.get(i).getCharacter();
                    }

                }
                tvCharactor.setText(charractor);
                for (int i = 0; i < arrCrew.size(); i++) {
                    if (i == 0) {
                        dirrector += arrCrew.get(i).getName();
                    } else {
                        dirrector += ", " + arrCrew.get(i).getName();
                    }
                }
                tvDirector.setText(dirrector);
                closeDialog();
            }

            @Override
            public void onFailure(Call<MovieCreditRespond> call, Throwable t) {
                closeDialog();
                Toast.makeText(DetailActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void checkAndPlay() {
        for (VideoLink item : mMovie.getLinks()) {
            if (item.isYoutubeLink()) {
                playYoutube(item.getKey());
                return;
            }
        }
    }

    private void playYoutube(final String key) {
        playerFragment.initialize(Constant.API_KEY_YT, new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, final YouTubePlayer youTubePlayer, boolean b) {
                youTubePlayer.cueVideo(key);

            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
                Toast.makeText(DetailActivity.this, "Can not load", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void getMovieDetail() {
        showDialog();
        ConnectServer.getResponseAPI().getMovieDetail(Constant.API_KEY, mMovie.getId()).enqueue(new Callback<MovieDetailRespond>() {
            @Override
            public void onResponse(Call<MovieDetailRespond> call, Response<MovieDetailRespond> response) {
                String json = new Gson().toJson(response.body());
                movie = ParserUtility.getBookDetail(json);
                tvName.setText(movie.getTitle());
                if (movie.getRelease_date() != null && movie.getRelease_date().length() > 5)
                    tvYear.setText(movie.getRelease_date().substring(0, 4));
                else
                    tvYear.setText("");
                tvOverview.setText(movie.getOverview());

                MySharedPreferences shares = new MySharedPreferences(getBaseContext());
                String tag = shares.getStringValue(Constant.TAGMOVIE + mMovie.getId());
                tvGenres.setText(tag);

                rb.setRating((float) movie.getVote_average() / 2);
                LayerDrawable stars = (LayerDrawable) rb.getProgressDrawable();
                stars.getDrawable(2).setColorFilter(getResources().getColor(android.R.color.holo_blue_bright), PorterDuff.Mode.SRC_ATOP);

                if (DatabaseUtility.checkExistsBook(getBaseContext(), String.valueOf(movie.getId()))) {
                    Log.e("", "DATA " + "data da co du lieu");
                } else {
                    DatabaseUtility.insertHistory(getBaseContext(),
                            mMovie);
                    MySharedPreferences share = new MySharedPreferences(getBaseContext());
                    share.putStringValue(Constant.DESC + movie.getId(), movie.getOverview());
//                    share.putStringValue(Constant.TAGMOVIE + movie.getId(), GlobalValue.genresToString(movie.getGenres()));
                }

                if (DatabaseUtility.checkExitsBookMarkinBook(getBaseContext(), mMovie.getId())) {
                    updateBookMark(true);
                } else {
                    updateBookMark(false);
                }
                closeDialog();
            }

            @Override
            public void onFailure(Call<MovieDetailRespond> call, Throwable t) {
                closeDialog();
                Toast.makeText(DetailActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void bookMakr() {
        Chapter chapter = new Chapter();
        chapter.setId(mMovie.getId());
        chapter.setBookId(mMovie.getId());
        chapter.setTitle(mMovie.getTitle());
        MySharedPreferences shares = new MySharedPreferences(getBaseContext());
//        if (!chapter.isBookMark(this)) {
        if (!shares.getBooleanValue(mMovie.getId())) {
            if (DatabaseUtility.addBookMark(this, chapter.getBookId(),
                    mMovie.getVoteAverage(), mMovie.getReleaseDate(), GlobalValue.genresIdsToString(mMovie.getGenreIds()),
                    mMovie.getTitle(), mMovie.getDesc(),
                    mMovie.getImage())) {
                updateBookMark(true);
                shares.putStringValue(Constant.TAGMOVIE + mMovie.getId(), GlobalValue.genresIdsToString(mMovie.getGenreIds()));
                shares.putBooleanValue(mMovie.getId(), true);
                Log.d("ABCDDDDDDD", "getView: " + mMovie.getId() + shares.getBooleanValue(mMovie.getId()));

            }
        } else {
            if (DatabaseUtility.removeBookMark(this,
                    chapter.getId())) {
                updateBookMark(false);
                shares.putBooleanValue(mMovie.getId(), false);
            }
        }
        return;
    }

    private void updateBookMark(boolean isBookMark) {
        if (isBookMark) {
            btnSave.setBackgroundResource(R.drawable.ic_favorite_active);
        } else {
            btnSave.setBackgroundResource(R.drawable.ic_favorite);
        }
    }
}
