package com.on.code.activity;

import android.os.Bundle;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.on.code.BaseActivity;
import com.on.code.R;
import com.on.code.config.Constant;
import com.on.code.config.GlobalValue;
import com.on.code.modelmanager.ParserUtility;
import com.on.code.network.ConnectServer;
import com.on.code.object.CatagoryObj;
import com.on.code.retrofitobj.GenreRespond;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends BaseActivity {
    private static final int SPLASH_DURATION = 3000;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Crashlytics.start(this);
        GlobalValue.constructor(this);
        setContentView(R.layout.activity_splash);
        // Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        GlobalValue.constructor(this);
        getData();


    }

    private void getData() {
        showDialog();
        ConnectServer.getResponseAPI().getGenre(Constant.API_KEY).enqueue(new Callback<GenreRespond>() {
            @Override
            public void onResponse(Call<GenreRespond> call, Response<GenreRespond> response) {
                String json = new Gson().toJson(response.body());
                ArrayList<CatagoryObj> arr = ParserUtility
                        .parseListCategory(json);

                GlobalValue.setCategory(arr);

                startActivity(MainSherkLockActivity.class);
                finish();
                closeDialog();
            }

            @Override
            public void onFailure(Call<GenreRespond> call, Throwable t) {
                Toast.makeText(SplashActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                closeDialog();
            }
        });
    }

}