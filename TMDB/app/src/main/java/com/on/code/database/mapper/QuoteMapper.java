package com.on.code.database.mapper;

import android.database.Cursor;

import com.on.code.database.CursorParseUtility;
import com.on.code.database.IRowMapper;
import com.on.code.object.Quote;

public class QuoteMapper implements IRowMapper<Quote> {
	@Override
	public Quote mapRow(Cursor row, int rowNum) {
		Quote chapter = new Quote();
		chapter.setId(CursorParseUtility.getString(row, "id"));
		chapter.setChapterId(CursorParseUtility.getString(row, "chapterId"));
		chapter.setQuoteIndex(CursorParseUtility.getString(row, "quoteIndex"));
		chapter.setContent(CursorParseUtility.getString(row, "content"));
		chapter.setImage(CursorParseUtility.getString(row, "image"));
		return chapter;
	}
}
