package com.on.code.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.on.code.R;
import com.on.code.config.GlobalValue;
import com.on.code.object.CatagoryObj;

public class CategoryAdapter extends BaseAdapter {

	private String TAG = "CategoryAdapter.java";
	private List<CatagoryObj> listCategory;
	private LayoutInflater inflater = null;
	private Activity act;

	public CategoryAdapter(Activity activity, List<CatagoryObj> listCategoryInfo) {
		this.listCategory = listCategoryInfo;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		act = activity;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return listCategory.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.row_item_category, null);
			holder = new ViewHolder();
			holder.lblTitle = (TextView) convertView
					.findViewById(R.id.titleList);

			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		CatagoryObj item = listCategory.get(position);
		if (item != null) {
			if (GlobalValue.preferences != null) {
				GlobalValue.setTypeFont(holder.lblTitle);
			}
			holder.lblTitle.setText(item.getTitle());
		} else {
			Log.i(TAG, "Null Object !");
		}
		return convertView;
	}

	private class ViewHolder {
		private TextView lblTitle;
	}

}
