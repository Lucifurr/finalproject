package com.on.code.object;

import android.app.Activity;

import com.on.code.database.DatabaseUtility;

public class BookMark {

	private String id, bookId, chapterId, chapterIndex, bookTitle,
			chapterTitle, image;
	private int quoteIndex;

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getBookTitle() {
		return bookTitle;
	}

	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}

	public String getChapterTitle() {
		return chapterTitle;
	}

	public void setChapterTitle(String chapterTitle) {
		this.chapterTitle = chapterTitle;
	}

	public int getQuoteIndex() {
		return quoteIndex;
	}

	public void setQuoteIndex(int quoteIndex) {
		this.quoteIndex = quoteIndex;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBookId() {
		return bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public String getChapterId() {
		return chapterId;
	}

	public void setChapterId(String chapterId) {
		this.chapterId = chapterId;
	}

	public String getChapterIndex() {
		return chapterIndex;
	}

	public void setChapterIndex(String chapterIndex) {
		this.chapterIndex = chapterIndex;
	}

	public Chapter getChaperInfo(Activity act) {
		return DatabaseUtility.getChapterById(act, chapterId);
	}


	public Movie convertToBook(){
		Movie movie = new Movie();
		movie.setId(bookId);
		movie.setTitle(bookTitle);
		movie.setImage(image);



		return movie;
	}

}
