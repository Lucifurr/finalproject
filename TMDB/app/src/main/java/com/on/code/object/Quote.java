package com.on.code.object;

import android.annotation.SuppressLint;

@SuppressLint("NewApi")
public class Quote {
	private String id;
	private String chapterId;
	private String quoteIndex;
	private String content, image;
	private int type;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getImage() {

		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getQuoteIndex() {
		return quoteIndex;
	}

	public void setQuoteIndex(String quoteIndex) {
		this.quoteIndex = quoteIndex;
	}

	public String getChapterId() {
		return chapterId;
	}

	public void setChapterId(String chapterId) {
		this.chapterId = chapterId;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

}
