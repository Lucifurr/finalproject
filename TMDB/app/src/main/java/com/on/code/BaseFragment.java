package com.on.code;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.on.code.widget.ProgressDialog;


public class BaseFragment extends Fragment {
    protected String TAG = this.getClass().getSimpleName();
    protected Activity self;
    private ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        self = getActivity();
    }
    protected void showDialog() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(self);
        }
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    protected void closeDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.hide();
        }
    }
}
