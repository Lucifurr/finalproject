package com.on.code.database;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import com.on.code.config.DatabaseConfig;
import com.on.code.database.binder.BookBinder;
import com.on.code.database.mapper.BookMapper;
import com.on.code.database.mapper.BookMarkMapper;
import com.on.code.database.mapper.ChapterMapper;
import com.on.code.database.mapper.QuoteMapper;
import com.on.code.object.Movie;
import com.on.code.object.BookMark;
import com.on.code.object.Chapter;
import com.on.code.object.Quote;

public final class DatabaseUtility {

	// history
	private static String STRING_SQL_INSERT_INTO_HISTORY = "INSERT OR IGNORE INTO "
			+ DatabaseConfig.TABLE_HISTORY + " VALUES(?,?,?,?,?,?,?,?)";

	public static ArrayList<Chapter> getListChaptersbyBook(Context context,
			String bookId) {
		PrepareStatement statement = new PrepareStatement(context);
		return statement.select(DatabaseConfig.TABLE_CHAPTER, "*", "bookId="
				+ bookId + " order by id", new ChapterMapper());
	}

	public static ArrayList<BookMark> getListBookMarkChapters(Context context) {
		PrepareStatement statement = new PrepareStatement(context);
		return statement.select(DatabaseConfig.TABLE_BOOK_MARK, "*",
				"chapterId>0 order by id DESC", new BookMarkMapper());
	}

	public static ArrayList<Chapter> searchChapterByTitleAndContent(
			Context context, String searchKey) {
		PrepareStatement statement = new PrepareStatement(context);
		return statement
				.select(DatabaseConfig.TABLE_CHAPTER,
						"*",
						"title like '%"
								+ searchKey
								+ "%' or id in (select chapterId from tblQuote where content like'%"
								+ searchKey + "%')", new ChapterMapper());
	}

	public static Chapter getChapterById(Context context, String chapterId) {
		PrepareStatement statement = new PrepareStatement(context);
		List<Chapter> arr = statement.select(DatabaseConfig.TABLE_CHAPTER, "*",
				"id=" + chapterId, new ChapterMapper());
		return arr.size() > 0 ? arr.get(0) : null;
	}

	public static ArrayList<Quote> getListQuotesByChapter(Context context,
			String chapterId) {
		PrepareStatement statement = new PrepareStatement(context);
		return statement.select(DatabaseConfig.TABLE_QUOTE, "*", "chapterId="
				+ chapterId, new QuoteMapper());
	}

	// ========= Movie mark

	public static boolean addBookMark(Context context, String bookId,
			double chapterIndex, String chapterId, String quotePosition,
			String bookTitle, String chapterTitle, String image) {
		PrepareStatement statement = new PrepareStatement(context);
		// if (!checkExitsBookMarkinBook(context, bookId)) {
		Object[] params = { bookId, chapterIndex, chapterId, quotePosition,
				bookTitle, chapterTitle, image };
		return statement
				.query("insert into "
						+ DatabaseConfig.TABLE_BOOK_MARK
						+ "(bookId,chapterIndex,chapterID,quotePosition,bookTitle,chapterTitle,image) values(?,?,?,?,?,?,?)",
						params);
		// } else {
		// return statement.update(DatabaseConfig.TABLE_BOOK_MARK,
		// "chapterIndex=" + chapterIndex + ",chapterId=" + chapterId
		// + ",quotePosition='" + quotePosition + "'",
		// "bookId=" + bookId);
		// }
	}

	public static boolean checkExitsBookMarkinBook(Context context,
			String bookId) {
		PrepareStatement statement = new PrepareStatement(context);
		ArrayList<BookMark> arr = statement.select(
				DatabaseConfig.TABLE_BOOK_MARK, "*", "bookId=" + bookId,
				new BookMarkMapper());

		return arr.size() > 0;
	}

	public static boolean checkChapterisBookMark(Context context,
			String bookId, String chapterId) {
		PrepareStatement statement = new PrepareStatement(context);
		ArrayList<BookMark> arr = statement.select(
				DatabaseConfig.TABLE_BOOK_MARK, "*", "bookId=" + bookId
						+ " and chapterId=" + chapterId, new BookMarkMapper());

		return arr.size() > 0;
	}

	public static BookMark getBookmarkById(Context context, String bookId,
			String chapterId) {
		PrepareStatement statement = new PrepareStatement(context);
		ArrayList<BookMark> arr = statement.select(
				DatabaseConfig.TABLE_BOOK_MARK, "*", "bookId=" + bookId
						+ " and chapterId=" + chapterId, new BookMarkMapper());

		return arr.size() > 0 ? arr.get(0) : null;
	}

	public static boolean removeBookMark(Context context,
			String chapterId) {
		PrepareStatement statement = new PrepareStatement(context);
		Object[] params = { chapterId };
		return statement.query("delete from " + DatabaseConfig.TABLE_BOOK_MARK
				+ " where bookID=? ", params);
	}

	public static boolean updateCurrentBookMarkIndex(Context context,
			String chapterId, String currentQuoteIndex) {
		PrepareStatement statement = new PrepareStatement(context);
		return statement.update(DatabaseConfig.TABLE_CHAPTER, "bookMarkIndex='"
				+ currentQuoteIndex + "'", "id=" + chapterId);
	}

	public static boolean clearBookMark(Context context) {
		try {
			PrepareStatement statement = new PrepareStatement(context);
			return statement.query("Delete from "
					+ DatabaseConfig.TABLE_BOOK_MARK, null);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	// ======================================HISTORY==================================
	// insert history
	public static boolean insertHistory(Context context, Movie movieObj) {
		PrepareStatement statement = new PrepareStatement(context);
		return statement.insert(STRING_SQL_INSERT_INTO_HISTORY, movieObj,
				new BookBinder());
	}

	// check exist movie
	public static boolean checkExistsBook(Context context, String bookId) {
		PrepareStatement statement = new PrepareStatement(context);
		ArrayList<Movie> movieInfo = new ArrayList<Movie>();
		movieInfo = statement.select(DatabaseConfig.TABLE_HISTORY, "*",
				"bookId =" + "'" + bookId + "'", new BookMapper());
		if (movieInfo.size() > 0)
			return true;
		else {
			return false;
		}
	}

	// list history
	public static ArrayList<Movie> getListHistory(Context context) {
		PrepareStatement statement = new PrepareStatement(context);
		return statement.select(DatabaseConfig.TABLE_HISTORY, "*", "",
				new BookMapper());
	}

	// clear history
	public static boolean clearHistory(Context context) {
		try {
			PrepareStatement statement = new PrepareStatement(context);
			return statement.query("Delete from "
					+ DatabaseConfig.TABLE_HISTORY, null);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}


	//==================================================

	public static boolean addBookMark(Context context, Movie movie) {
		PrepareStatement statement = new PrepareStatement(context);
		return statement.insert(STRING_SQL_INSERT_INTO_HISTORY, movie,
				new BookBinder());
	}
}
