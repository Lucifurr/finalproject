package com.on.code.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.on.code.BaseActivity;
import com.on.code.R;
import com.on.code.adapter.MovieGridviewAdapter;
import com.on.code.config.Constant;
import com.on.code.config.GlobalValue;
import com.on.code.database.DatabaseUtility;
import com.on.code.modelmanager.ParserUtility;
import com.on.code.network.ConnectServer;
import com.on.code.object.Movie;
import com.on.code.pulltorefresh.library.PullToRefreshBase;
import com.on.code.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.on.code.pulltorefresh.library.PullToRefreshGridView;
import com.on.code.retrofitobj.MovieInGenreRespond;
import com.on.code.util.MySharedPreferences;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieByCategoryActivity extends BaseActivity implements
        OnClickListener {
    private GridView grvBook;
    private PullToRefreshGridView mGridView;
    private MovieGridviewAdapter grv_adapter;
    private Activity self;
    private ArrayList<Movie> arrMovies;
    private int currentPage = 1;
    private int totalPage;
    private boolean hasMoreData;// check if there is more data or not
    private int currentView;
    private final int GRID_VIEW = 2;
    ProgressDialog pDialog;
    private String categoryId, cateTitle = "";
    // header
    private TextView lblTitle, lblNoData;
    private ImageView btnTopMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_by_category);
        self = MovieByCategoryActivity.this;
        Bundle b = getIntent().getExtras();
        categoryId = b.getString("categoryId");
        cateTitle = b.getString("cateTitle");

        initUI();
        initControl();
        hasMoreData = true;
        currentPage = 1;
        arrMovies.clear();
        setData(true, currentView);
    }

    private void initUI() {
        lblNoData = (TextView) findViewById(R.id.lblNoData);
        lblTitle = (TextView) findViewById(R.id.lblTopTitle);
        btnTopMenu = (ImageView) findViewById(R.id.btnTopLeft);
        lblTitle.setText(cateTitle);

        mGridView = (PullToRefreshGridView) findViewById(R.id.grvBook);
        grvBook = mGridView.getRefreshableView();
        mGridView.setOnRefreshListener(new OnRefreshListener2<GridView>() {

            @Override
            public void onPullDownToRefresh(
                    PullToRefreshBase<GridView> refreshView) {
                currentView = GRID_VIEW;
                hasMoreData = true;
                currentPage = 1;
                arrMovies.clear();
                setData(true, currentView);

            }

            @Override
            public void onPullUpToRefresh(
                    PullToRefreshBase<GridView> refreshView) {
                currentView = GRID_VIEW;
                currentPage++;
                if (currentPage > totalPage) {
                    hasMoreData = false;
                }
                setData(false, currentView);
            }

        });
    }

    private void initControl() {
        btnTopMenu.setOnClickListener(this);
        setGridView();
    }

    private void setGridView() {
        arrMovies = new ArrayList<Movie>();
        grv_adapter = new MovieGridviewAdapter(self, arrMovies);
        grvBook.setAdapter(grv_adapter);
        grvBook.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub

                if (DatabaseUtility.checkExistsBook(MovieByCategoryActivity.this, arrMovies
                        .get(position).getId())) {
                    Log.e("", "DATA " + "data da co du lieu");
                } else {
                    DatabaseUtility.insertHistory(MovieByCategoryActivity.this,
                            arrMovies.get(position));
                    MySharedPreferences share = new MySharedPreferences(MovieByCategoryActivity.this);
                    share.putStringValue(Constant.DESC + arrMovies.get(position).getId(), arrMovies.get(position).getDesc());
                    share.putStringValue(Constant.TAGMOVIE + arrMovies.get(position).getId(), GlobalValue.genresIdsToString(arrMovies.get(position).getGenreIds()));
                }

                GlobalValue.movie = arrMovies.get(position);
                GlobalValue.imagebook = arrMovies.get(position).getImage();
                Bundle b = new Bundle();
                b.putParcelable(DetailActivity.KEY_OBJECT, arrMovies.get(position));

                startActivity(DetailActivity.class, b);
            }
        });
    }

    private void setData(boolean isRefresh, int mode) {
        if (hasMoreData) {
            showDialog();
            ConnectServer.getResponseAPI().getMovieInGenre(categoryId, Constant.API_KEY).enqueue(new Callback<MovieInGenreRespond>() {
                @Override
                public void onResponse(Call<MovieInGenreRespond> call, Response<MovieInGenreRespond> response) {
                    String json = new Gson().toJson(response.body());
                    ArrayList<Movie> arr = ParserUtility
                            .parseListNewBooks(json);
                    totalPage = ParserUtility.getTotalBookPage(json);
                    arrMovies.addAll(arr);
                    grv_adapter.notifyDataSetChanged();
                    mGridView.onRefreshComplete();

                    if (arrMovies.size() == 0)
                        lblNoData.setVisibility(View.VISIBLE);
                    else
                        lblNoData.setVisibility(View.GONE);
                    closeDialog();
                }

                @Override
                public void onFailure(Call<MovieInGenreRespond> call, Throwable t) {
                    mGridView.onRefreshComplete();
                    closeDialog();
                    Toast.makeText(self, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(self, "No more data to show.", Toast.LENGTH_SHORT)
                    .show();
            mGridView.onRefreshComplete();

        }
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        if (v == btnTopMenu) {
            onBackPressed();
            return;
        }
    }
}
