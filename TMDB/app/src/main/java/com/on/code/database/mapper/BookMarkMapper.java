package com.on.code.database.mapper;

import android.database.Cursor;

import com.on.code.database.CursorParseUtility;
import com.on.code.database.IRowMapper;
import com.on.code.object.BookMark;

public class BookMarkMapper implements IRowMapper<BookMark> {
	@Override
	public BookMark mapRow(Cursor row, int rowNum) {
		BookMark book = new BookMark();
		book.setId(CursorParseUtility.getString(row, "id"));
		book.setBookId(CursorParseUtility.getString(row, "bookId"));
		book.setChapterId(CursorParseUtility.getString(row, "chapterId"));
		book.setChapterIndex(CursorParseUtility.getString(row, "chapterIndex"));
		book.setQuoteIndex(CursorParseUtility.getInt(row, "quotePosition"));
		book.setBookTitle(CursorParseUtility.getString(row, "bookTitle"));
		book.setChapterTitle(CursorParseUtility.getString(row, "chapterTitle"));
		book.setImage(CursorParseUtility.getString(row, "image"));
		return book;
	}
}
